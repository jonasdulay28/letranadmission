<?php
class Admin_model extends MY_Model{

	public function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Manila');

	}

	public function get_feedbacks(){
		$query = $this->db->select("a.*,b.first_name as first_name,b.last_name as last_name,b.middle_name as middle_name,b.email as email")->from("feedback a")
		->join('users b', 'b.reference_number = a.reference_number')->get();
		return $query->result();
	}

	public function get_submitted_exams() {
		$query = $this->db->select("a.id as id,a.updated_at as updated_at,b.first_name as first_name,b.last_name as last_name,b.middle_name as middle_name")->from("answers a")
		->join('users b', 'b.reference_number = a.reference_number')
		->where('a.status',1)->get();
		return $query->result();
	}

	public function get_reports() {
		$query = $this->db->select("count(a.application_id) as data, b.citymunDesc as label")
		->from("application a")->join('refcitymun b','b.citymunCode = a.city','left')->group_by('a.city')->get();
		return $query->result();
	}

	public function get_reports2($from,$to) {
		$query = $this->db->select("count(a.application_id) as data, b.citymunDesc as label")
		->from("application a")->where(array('created_at >='=>$from,"created_at <=" => $to))->join('refcitymun b','b.citymunCode = a.city','left')->group_by('a.city')->get();
		return $query->result();
	}
	public function get_reports_bar() {
		$query = $this->db->select("count(a.application_id) as data, b.citymunDesc as label")
		->from("application a")->join('refcitymun b','b.citymunCode = a.city','left')->group_by('a.city')->get();
		return $query->result();
	}
}