<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedbacks extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$this->load->model('Admin_model');
	}

	public function index()
	{
		$this->load->view('templates/admin_login_template');
	}

	public function get_feedbacks() {
		$data['feedbacks'] = $this->Admin_model->get_feedbacks();
		echo json_encode($data);
	}

	public function get_application() {
		$application_id = clean_data(rawurldecode(get('q')));
		$filter = ["application_id"=>$application_id];
		$data["application"] = $this->Admin_model->fetch_data('application',$filter);
		echo json_encode($data);
	}

	public function edit()
	{
		$response = ["message"=>"success"];
		$application_data = json_decode(post('application_data'));
		$application_data->apply_reason = implode(',',$application_data->apply_reason);
		$application_data->referal= implode(',',$application_data->referal);
		$application_id = clean_data(post('application_id'));
		$filter = ["application_id"=>$application_id]; 
		$this->Admin_model->update('application',$application_data,$filter);
		echo json_encode($response);
	}

	public function delete()
	{
		
	}

	
}