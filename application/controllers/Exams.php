<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exams extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$this->load->model("Admin_model");
	}

	public function index(){
		if(isset($this->session->permit_key) && $this->session->permit_key != ""){
			$data['questions'] = $this->Admin_model->fetch('questions');
			$data['subjects'] = [];
			$data['questions_desc'] = [];
			$data['questions_choices'] = [];
			$data['courses'] = [];
			foreach($data['questions'] as $val){
				array_push($data['subjects'], $val->subject_name);
				array_push($data['questions_desc'], $val->question_desc);
				array_push($data['questions_choices'], $val->question_choices);
				array_push($data['courses'], $val->courses);
			}
			$this->load->view('templates/exam_template',$data);
		}
	}


	public function add()
	{
		$response["message"] = "error";
		$subject_name = clean_data(post('subject_name'));
		$data = ["subject_name"=>$subject_name];
		$check_exist = $this->Admin_model->fetch_tag_row("*","subjects",$data);
		if(empty($check_exist)){
			$this->Admin_model->insert('subjects',$data);
			$response["message"] = "success";
		}
		echo json_encode($response);
	}

	public function get_subjects() {
		$data['subjects'] = $this->Admin_model->fetch('subjects');
		echo json_encode($data);
	}

	public function get_submitted_exams(){
		$data["submitted_exams"] = $this->Admin_model->get_submitted_exams();
		echo json_encode($data);
	}

	public function view_exam(){
		$id = clean_data(post('id'));
		$filter = ["id"=>$id];
		$row = $this->Admin_model->fetch_tag_row('*','answers',$filter);

		$course_suggestion = json_decode($row->course_suggestion);
		$tmp_course_suggestion = [];
		$x= 0;
		foreach($course_suggestion as $val){
			if($x < 2) {
				$filter = ["course_id"=>$val];
				$tmp_course = $this->Admin_model->fetch_tag_row("course_name","course",$filter);
				array_push($tmp_course_suggestion,$tmp_course->course_name);
			}
			$x++;
		}
		$row->course_suggestion = $tmp_course_suggestion;

		echo json_encode(["exam" => $row]);
	}

	public function edit()
	{
		$response = ["message"=>"success"];
		$subject_name = clean_data(post('subject_name'));
		$questions = post('questions');
		$choices = post('choices');
		$courses = post('courses');
		$answers = post('answers');
		$filter = ["subject_name"=>$subject_name];
		$check_exist = $this->Admin_model->fetch_tag_row("*","questions",$filter);
		if(empty($check_exist)){
			$data = ["question_desc"=>$questions,"question_answer"=>$answers,"question_choices"=>$choices,"subject_name"=>$subject_name,"courses"=>$courses];
			$this->Admin_model->insert('questions',$data);
			$response["message"] = "success";
		}else{
			$data = ["question_desc"=>$questions,"question_answer"=>$answers,"question_choices"=>$choices,"courses"=>$courses];
			$this->Admin_model->update('questions',$data,$filter);
		}
		echo json_encode($response);
	}

	public function getQuestions() {
		$subject_name = clean_data(post('subject_name'));
		$filter = ["subject_name"=>$subject_name];
		$data['questions'] = $this->Admin_model->fetch_tag_row("*","questions",$filter);
		echo json_encode($data);
	}

	public function delete()
	{
		$response = ["message"=>"success"];
		$subject_name = clean_data(post('subject_name'));
		$filter = ["subject_name"=>$subject_name]; 
		$this->Admin_model->delete('questions',$filter);
		$this->Admin_model->delete('subjects',$filter);
		echo json_encode($response);
	}

	public function save_exam(){
		$answers = json_encode(post('answer'));
		$subjects = json_encode(post('subject'));
		$courses = json_encode(post('courses'));
		$questions = json_encode(post('question'));
		$choices = json_encode(post('choices'));
		$reference_number = decrypt($this->session->ses_id);

		$data = ["student_answer"=>$answers];
		$filter = ["reference_number"=>$reference_number]; 
		$check_exist = $this->Admin_model->check_exist("answers",$filter);
		
		//save to database 	
		if($check_exist < 1){
			$data["reference_number"] = $reference_number;
			$this->Admin_model->insert("answers",$data);
		}else {
			$this->Admin_model->update("answers",$data,$filter);
		}

		echo json_encode(["message"=>"success"]);
	}

	function itexmo($number,$message,$apicode){
		$url = 'https://www.itexmo.com/php_api/api.php';
		$itexmo = array('1' => $number, '2' => $message, '3' => $apicode);
		$param = array(
		    'http' => array(
		        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
		        'method'  => 'POST',
		        'content' => http_build_query($itexmo),
		    ),
		);
		$context  = stream_context_create($param);
		return file_get_contents($url, false, $context);
		}

	public function submit_exam(){
		$answers = json_encode(post('answer'));
		$subjects = json_encode(post('subject'));
		$courses = json_encode(post('courses'));
		$questions = json_encode(post('question'));
		$choices = json_encode(post('choices'));
		$reference_number = decrypt($this->session->ses_id);
		
		//check answers
		$exam = $this->Admin_model->fetch_tag("question_answer","questions");
		$question_answer = [];
		foreach($exam as $val){
			$tmp_answers = json_decode($val->question_answer);
			$question_answer = array_merge($question_answer, $tmp_answers);
		}

		$student_answer = post('answer');

		$score = 0;
		$x = 0;
		$tmp_courses = post('courses');
		$tmp_course_suggestion = [];
		//get score
		foreach($student_answer as $val) {
			if($val == $question_answer[$x]){
				$score += 1;
				array_push($tmp_course_suggestion, $tmp_courses[$x]);
			}
			$x++;
		} 

		$course_suggestion = [];
		foreach($tmp_course_suggestion as $val){
			$tmp = explode(",",$val);
			foreach($tmp as $val2) {
				array_push($course_suggestion, $val2);
			} 
		}

		//get course suggestion
		$counted = array_count_values($course_suggestion); //get the number of occurences
		arsort ($counted); //sort to descending
		$course_suggestion = json_encode(array_keys($counted)); //re-initialize

		$data = ["question_desc"=>$questions,"question_choices"=>$choices,"courses"=>$courses,"course_suggestion"=>$course_suggestion
		,"subject_name"=>$subjects,"student_answer"=>$answers,"score"=>$score,"question_answer"=>json_encode($question_answer)];
		$filter = ["reference_number"=>$reference_number]; 
		$check_exist = $this->Admin_model->check_exist("answers",$filter);
		
		//save to database 	
		if($check_exist < 1){
			$data["reference_number"] = $reference_number;
			$this->Admin_model->insert("answers",$data);
		}else {
			$data["status"] = 1;
			$this->Admin_model->update("answers",$data,$filter);
		}

		$this->Admin_model->delete("permit_key",$filter);
		redirect(base_url('Exams/exam_success'));
	}

	public function exam_success() {
		$reference_number = decrypt($this->session->ses_id);
		$filter = ["reference_number"=>$reference_number]; 
		$row = $this->Admin_model->fetch_tag_row("score, course_suggestion","answers",$filter);
		$course_suggestion = json_decode($row->course_suggestion);
		$data["score"] = $row->score; 
		$tmp_course_suggestion = [];
		$x= 0;
		foreach($course_suggestion as $val){
			if($x < 2) {
				$filter = ["course_id"=>$val];
				$tmp_course = $this->Admin_model->fetch_tag_row("course_name","course",$filter);
				array_push($tmp_course_suggestion,$tmp_course->course_name);
			}
			$x++;
		}
		$data["course_suggestion"] = $tmp_course_suggestion;
		$filter = ["reference_number"=>$reference_number]; 
		$row = $this->Admin_model->fetch_tag_row("mobile","application",$filter);
		$number = $row->mobile;

		$apply_status = ($data["score"] < 33 ? 'Probationary' : 'Regular');
		$mes_course_suggestion = implode(", ",$tmp_course_suggestion);
		$mes_course_suggestion = str_replace("Bachelor of Science","BS",$mes_course_suggestion);
		$apicode = "TR-KARLJ862391_1JF76";
		$message = "Exam Result: ".$apply_status."\nCourse Suggestion: ".$mes_course_suggestion;
		$result = $this->itexmo($number,$message,"TR-KARLJ862391_1JF76");
		if ($result == ""){
			/*echo "iTexMo: No response from server!!!
			Please check the METHOD used (CURL or CURL-LESS). If you are using CURL then try CURL-LESS and vice versa.	
			Please CONTACT US for help. ";	*/
		}else if ($result == 0){
			//echo "Message Sent!";
		}
		else{	
			//echo "Error Num ". $result . " was encountered!";
		}
		$this->load->view("templates/exam_success",$data);
	}

	public function test(){
		/*$apicode = "TR-KARLJ251821_HULKC";
		$apply_status = "asd";
		$mes_course_suggestion = "asdweqr";
		$reference_number = decrypt($this->session->ses_id);
		$filter = ["reference_number"=>$reference_number]; 
		$row = $this->Admin_model->fetch_tag_row("mobile","application",$filter);
		$number = $row->mobile;
		$message = "
			Thank you for taking our Entrance Exam<br><br>
			Exam Result: ".$apply_status."<br>
			Course Suggestion: ".$mes_course_suggestion."<br><br>
			";
			$result = $this->itexmo($number,$message,$apicode);
			var_dump($result);*/
			$result = $this->itexmo("09206008945","Test Message","TR-KARLJ251821_HULKC");
			if ($result == ""){
			echo "iTexMo: No response from server!!!
			Please check the METHOD used (CURL or CURL-LESS). If you are using CURL then try CURL-LESS and vice versa.	
			Please CONTACT US for help. ";	
			}else if ($result == 0){
			echo "Message Sent!";
			}
			else{	
			echo "Error Num ". $result . " was encountered!";
			}
	}

	public function save_setting(){
		$data = $_POST;
		$filter = ["id"=>1];
		$this->Admin_model->update("exam_settings",$data,$filter);
		echo json_encode(["message"=>"success"]);
	}
	public function get_settings() {
		$data["settings"] = $this->Admin_model->fetch('exam_settings');
		echo json_encode($data);
	}

	public function get_Score($student_answer,$question_answer){
		$score = 0;
		$x = 0;
		foreach($student_answer as $val) {
			if($val == $question_answer[$x]){
				$score += 1;
			}
			$x++;
		} 
		return $score;

	}
	
}