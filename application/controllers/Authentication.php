<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$this->load->model("User_model");
	}

	public function index()
	{
		$this->load->view('templates/authentication_template');
	}

	public function login()
	{
		$response = ["message"=>"error"];
		$filter = [];
		foreach ($_POST as $key => $value) {
			$filter[$key] = clean_data($value);
		}
		//check if activated or not
		$filter["status"] = 1;
		//check if user exist
		$result = $this->User_model->fetch_tag_row("*","users",$filter,"1");
		if(!empty($result)){
			//set session
			$this->session->set_userdata('ses_id',encrypt($filter['reference_number']));
			$response["message"] = "success";
		}
		echo json_encode($response);
	}


	public function add()
	{

	}

	public function edit()
	{
		
	}

	public function delete()
	{
		
	}

	
}