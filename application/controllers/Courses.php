<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Courses extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$this->load->model('Admin_model');
	}

	public function get_courses() {
		$data['course'] = $this->Admin_model->fetch('course');
		echo json_encode($data);
	}


	public function add()
	{
		$response = ["message"=>"success"];
		$course_data = json_decode(post('course_data'));
		$this->Admin_model->insert('course',$course_data);
		echo json_encode($response);
	}

	public function get_course(){
		$course_id = clean_data(rawurldecode(get('q')));
		$filter = ["course_id"=>$course_id];
		$data["course"] = $this->Admin_model->fetch_data('course',$filter);
		echo json_encode($data);
	}

	public function edit()
	{
		$response = ["message"=>"success"];
		$course_data = json_decode(post('course_data'));
		$course_id = clean_data(post('course_id'));
		$filter = ["course_id"=>$course_id]; 
		$this->Admin_model->update('course',$course_data,$filter);
		echo json_encode($response);
	}

	public function delete()
	{
		$course_id = clean_data(post('course_id'));
		$filter = ["course_id"=>$course_id]; 
		$this->Admin_model->delete('course',$filter);
		echo json_encode($response);
	}
	
}