<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$this->load->model("Admin_model");
	}

	public function get_reports() {
		$data['reports'] = $this->Admin_model->get_reports();
		echo json_encode($data);
	}

	public function get_reports_bar() {
		
		$tmp_reports = $this->Admin_model->get_reports_bar();
		$data['reports'] = [];
		
		foreach($tmp_reports as $key) {
			array_push($data['reports'], [$key->label,$key->data]);
		}
		echo json_encode($data);
	}

	public function print_report() {
		$from =get('from').' 00:00:00';
		$to = get('to').' 23:59:59';
		$data['reports'] = $this->Admin_model->get_reports2($from,$to);
		$this->load->view('templates/print_template',$data);
	}

}