<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Applications extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$this->load->model('Admin_model');
	}

	public function index()
	{
		$this->load->view('templates/admin_login_template');
	}

	public function login()
	{
		$data['content'] = 'pages/login';
		$this->load->view('templates/authentication_template',$data);
	}

	public function dashboard(){
		$this->load->view('templates/admin_template');
	}


	public function get_applications() {
		$data['applications'] = $this->Admin_model->fetch('application');
		echo json_encode($data);
	}

	public function get_application() {
		$application_id = clean_data(rawurldecode(get('q')));
		$filter = ["application_id"=>$application_id];
		$data["application"] = $this->Admin_model->fetch_data('application',$filter);
		echo json_encode($data);
	}


	public function register()
	{
		$data['content'] = 'pages/register';
		$this->load->view('templates/authentication_template',$data);
	}

	public function add()
	{

	}

	public function generatePermit() {
		$response = ["message"=>"success"];
		$reference_number = clean_data(post('reference_number'));
		$filter = ["reference_number"=>$reference_number];
		$this->db->delete('permit_key',$filter);
		$permit_key = "";
		{
			//regenerate id if exists
			$permit_key =  $this->getRandomWord();
			$filter=array("permit_key"=>$permit_key);
			$check_exist= $this->Admin_model->check_exist("permit_key",$filter);
		}while($check_exist > 0);
		$data = ["reference_number"=>$reference_number,'permit_key'=>$permit_key];
		$this->db->insert('permit_key',$data);
		$response['permit_key'] = $permit_key;
		echo json_encode($response);
	}

	public function getRandomWord($len = 5) {
	    $word = array_merge(range('a', 'z'), range('A', 'Z'));
	    shuffle($word);
	    return substr(implode($word), 0, $len);
	}

	public function edit()
	{
		$response = ["message"=>"success"];
		$application_data = json_decode(post('application_data'));
		$application_data->apply_reason = implode(',',$application_data->apply_reason);
		$application_data->referal= implode(',',$application_data->referal);
		$application_id = clean_data(post('application_id'));
		$filter = ["application_id"=>$application_id]; 
		$this->Admin_model->update('application',$application_data,$filter);
		echo json_encode($response);
	}

	public function tagApplication()
	{
		$response = ["message"=>"success"];
		$application_id = clean_data(post('application_id'));
		$email = clean_data(post('email'));
		$remarks = clean_data(post('remarks'));
		$status = clean_data(post('status'));
		$filter = ["application_id"=>$application_id]; 
		$data = ["status"=>$status];
		$this->Admin_model->update('application',$data,$filter);

		//send email
		$this->load->helper('custom_helper');
		$email_content = array("message"=>$remarks,"from"=>"letranadmission@gmail.com","from_name"=>"San Juan de Letran Calamba","to"=>$email,"subject"=>"Remarks - Letran Calamba");
		$status = sendMailCIMailer($email_content);

		echo json_encode($response);
	}

	public function delete()
	{
		
	}

	
}