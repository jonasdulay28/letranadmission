<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$this->load->model("User_model");
		if(isset($this->session->ses_id)){
			redirect(base_url('Dashboard'));
		}
	}

	public function index()
	{
		$this->load->view('templates/homepage_template');
	}

	public function add()
	{

	}

	public function edit()
	{
		
	}

	public function delete()
	{
		
	}

	
}