<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admission extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$this->load->model('Admission_model');
		if(isset($this->session->ses_id)){
			redirect(base_url('Dashboard'));
		}
	}

	public function index()
	{
		$this->load->view('templates/admission_template');
	}

	public function apply()
	{
		if($_POST){
			//insert data to application table
			$applicant_data = [];
			foreach($_POST as $key => $value){
 				if($key == "Certification" ||  $key == "submit_" || $key== "Certification2"){
				
				}else{
					if($key == "apply_reason") {
						$applicant_data[$key] = strtoupper(implode(',',$value));
					}elseif($key == "referal"){
						$applicant_data[$key] = strtoupper(implode(',',$value));
					}
					else {
						$applicant_data[$key] = strtoupper(clean_data($value));
					}
				}
			}
			if(!empty($_FILES['id_picture']['size'])) {
				// where are you storing it
		        $path = './uploads';
		        // what are you naming it
		        $new_name = 'id_picture' . time();
		        // start upload
		        $input = 'id_picture';
		        $result = $this->upload_image($path, $new_name, $input);
		        $ext = pathinfo($_FILES['id_picture']['name'], PATHINFO_EXTENSION);
		        $applicant_data["id_picture"] = $new_name.".".$ext;
			}
			if(!empty($_FILES['certificate_of_candidacy']['size'])) {
				// where are you storing it
		        $path = './uploads';
		        // what are you naming it
		        $new_name = 'certificate_of_candidacy' . time();
		        // start upload
		        $input = 'certificate_of_candidacy';
		        $result = $this->upload_image($path, $new_name, $input);
		        $ext = pathinfo($_FILES['certificate_of_candidacy']['name'], PATHINFO_EXTENSION);
		        $applicant_data["certificate_of_candidacy"] = $new_name.".".$ext;
			}
			if(!empty($_FILES['nso_psa']['size'])) {
				// where are you storing it
		        $path = './uploads';
		        // what are you naming it
		        $new_name = 'nso_psa' . time();
		        // start upload
		        $input = 'nso_psa';
		        $result = $this->upload_image($path, $new_name, $input);
		        $ext = pathinfo($_FILES['nso_psa']['name'], PATHINFO_EXTENSION);
		        $applicant_data["nso_psa"] = $new_name.".".$ext;
			}
			if(!empty($_FILES['report_card']['size'])) {
				// where are you storing it
		        $path = './uploads';
		        // what are you naming it
		        $new_name = 'report_card' . time();
		        // start upload
		        $input = 'report_card';
		        $result = $this->upload_image($path, $new_name, $input);
		        $ext = pathinfo($_FILES['report_card']['name'], PATHINFO_EXTENSION);
		        $applicant_data["report_card"] = $new_name.".".$ext;
			}

			//create account for applicant
			$reference_number =  "";
			{
				//regenerate id if exists
				$reference_number = date('Y').mt_rand(10000,99999);
				$filter=array("reference_number"=>$reference_number);
				$check_exist= $this->Admission_model->check_exist("users",$filter);
			}while($check_exist > 0);

			//insert to application table
			$applicant_data["reference_number"] = $reference_number;
			$this->Admission_model->insert('application',$applicant_data);
			$tmp_lname = explode(' ',trim($applicant_data["last_name"]));
			$password = strtolower(substr($applicant_data["first_name"], 0, 1).$tmp_lname[0]);

			$users_data = ["reference_number"=>$reference_number,"password"=>$password,"first_name"=>$applicant_data["first_name"],
			"last_name"=>$applicant_data["last_name"],"middle_name"=>$applicant_data["middle_name"],"suffix"=>$applicant_data["suffix"],
			"status"=>1,"role"=>"Student","email"=>$applicant_data["email"],"contact_number"=>$applicant_data["mobile"]
			];
			
			$this->Admission_model->insert('users',$users_data);
			$this->session->set_flashdata('reference_number', $reference_number);
			$this->session->set_flashdata('password', $password);

			$this->load->helper('custom_helper');
			
			redirect(base_url('Admission/success'));
		}else{
			$data["province"] = $this->Admission_model->fetch('refprovince','','','','provDesc ASC');
			$data["city"] = $this->Admission_model->fetch('refcitymun','','','','citymunDesc ASC');
			
			$this->load->view('templates/apply_template',$data);
		}
		
	}

	function get_brgy() {
		$q = clean_data(get('q'));
		$filter = ["citymunCode"=>$q];
		$data["brgy"] = $this->Admission_model->fetch('refbrgy',$filter,'','','brgyDesc ASC');
		echo json_encode($data);
	}


	public function reupload_documents() {
		die("test");
	}
	function upload_image($path, $new_name, $input) {
	    // define parameters
	    $config['upload_path'] = $path;
	    $config['allowed_types'] = 'jpg|png|pdf|docx';
	    $config['max_size'] = '0';
	    $config['max_width'] = '0';
	    $config['max_height'] = '0';
	    $config['file_name'] = $new_name;
	    $this->load->library('upload');
	    $this->upload->initialize($config);
	    // upload the image
	    if ($this->upload->do_upload($input)) {
	        // success
	        // pass back $this->upload->data() for info
	    } else {
	        // failed
	        // pass back $this->upload->display_errors() for info
	    }
	}

	public function success(){
		$this->load->view('templates/apply_success');
	}

	public function login()
	{
		$data['content'] = 'pages/login';
		$this->load->view('templates/authentication_template',$data);
	}



	public function register()
	{
		$data['content'] = 'pages/register';
		$this->load->view('templates/authentication_template',$data);
	}

	public function add()
	{

	}

	public function edit()
	{
		
	}

	public function delete()
	{
		
	}

	public function handleImage()
	{
		if (isset($_FILES['post_file']) && !empty($_FILES['post_file']['name'])):
	        if ($this->upload->do_upload('post_file')):
	        	// $this->session->set_userdata('p_img1',$this->upload->data());
	            return true;
	        else:
	        	$this->form_validation->set_message('handleImage', $this->upload->display_errors());
	            return false;
	        endif;
	    endif;
	}

	
}