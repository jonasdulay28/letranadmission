<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$this->load->model("Admin_model");

		if(!isset($this->session->ses_id)){
			redirect(base_url('Admission'));
		}
	}

	public function reupload_documents() {
			$applicant_data = [];
			$reference_number = decrypt($this->session->ses_id);
			$filter= ["reference_number"=>$reference_number];
			if(!empty($_FILES['id_picture']['size'])) {
				// where are you storing it
		        $path = './uploads';
		        // what are you naming it
		        $new_name = 'id_picture' . time();
		        // start upload
		        $input = 'id_picture';
		        $result = $this->upload_image($path, $new_name, $input);
		        $ext = pathinfo($_FILES['id_picture']['name'], PATHINFO_EXTENSION);
		        $applicant_data["id_picture"] = $new_name.".".$ext;
			}
			if(!empty($_FILES['certificate_of_candidacy']['size'])) {
				// where are you storing it
		        $path = './uploads';
		        // what are you naming it
		        $new_name = 'certificate_of_candidacy' . time();
		        // start upload
		        $input = 'certificate_of_candidacy';
		        $result = $this->upload_image($path, $new_name, $input);
		        $ext = pathinfo($_FILES['certificate_of_candidacy']['name'], PATHINFO_EXTENSION);
		        $applicant_data["certificate_of_candidacy"] = $new_name.".".$ext;
			}
			if(!empty($_FILES['nso_psa']['size'])) {
				// where are you storing it
		        $path = './uploads';
		        // what are you naming it
		        $new_name = 'nso_psa' . time();
		        // start upload
		        $input = 'nso_psa';
		        $result = $this->upload_image($path, $new_name, $input);
		        $ext = pathinfo($_FILES['nso_psa']['name'], PATHINFO_EXTENSION);
		        $applicant_data["nso_psa"] = $new_name.".".$ext;
			}
			if(!empty($_FILES['report_card']['size'])) {
				// where are you storing it
		        $path = './uploads';
		        // what are you naming it
		        $new_name = 'report_card' . time();
		        // start upload
		        $input = 'report_card';
		        $result = $this->upload_image($path, $new_name, $input);
		        $ext = pathinfo($_FILES['report_card']['name'], PATHINFO_EXTENSION);
		        $applicant_data["report_card"] = $new_name.".".$ext;
			}

			if(empty($applicant_data)){
				redirect(base_url('Dashboard'));
			}else {
				$this->Admin_model->update("application",$applicant_data,$filter);
				redirect(base_url('Dashboard/reupload_success'));
			}

			
	}

	function reupload_success() {
		$this->load->view('templates/reupload');
	}

	function upload_image($path, $new_name, $input) {
	    // define parameters
	    $config['upload_path'] = $path;
	    $config['allowed_types'] = 'jpg|png|pdf|docx';
	    $config['max_size'] = '0';
	    $config['max_width'] = '0';
	    $config['max_height'] = '0';
	    $config['file_name'] = $new_name;
	    $this->load->library('upload');
	    $this->upload->initialize($config);
	    // upload the image
	    if ($this->upload->do_upload($input)) {
	        // success
	        // pass back $this->upload->data() for info
	    } else {
	        // failed
	        // pass back $this->upload->display_errors() for info
	    }
	}

	public function index()
	{
		$this->load->view('templates/dashboard_template');
	}

	public function add()
	{

	}

	public function edit()
	{
		
	}

	public function delete()
	{
		
	}

	public function logout(){
		session_destroy();
		redirect(base_url('Admission'));
	}

	public function add_feedback() {
		$feedback = clean_data(post('feedback'));
		$reference_number = decrypt($this->session->ses_id);
		$data = ["feedback"=>$feedback,"reference_number"=>$reference_number];
		$this->db->insert('feedback',$data);
		echo json_encode(["message"=>"success"]);
	}

	public function get_dashboard_data(){
		$data["num_applications"] = $this->Admin_model->count_rows("application");
		$data["num_users"] = $this->Admin_model->count_rows("users");
		$data["num_courses"] = $this->Admin_model->count_rows("course");
		$data["num_exam"] = $this->Admin_model->count_rows("exam");
		echo json_encode($data);
	}

	public function validate_key(){
		$response= ["message"=> "success"];
		$permit_key = clean_data(post('permit_key'));
		$reference_number = decrypt($this->session->ses_id);
		$filter = ["permit_key"=>$permit_key,"reference_number"=>$reference_number];

		$check_exist = $this->Admin_model->count_rows("permit_key",$filter);
		if($check_exist < 1){
			$response["message"] = "error";
		}else {
			$this->session->set_userdata("permit_key",$permit_key);
		}
		echo json_encode($response);
	}
}