<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$this->load->model('Admin_model');
	}

	public function test() {
		$status = sendMail("asdsa","sadas","jonasdulay28@gmail.com","jonasdulay28@gmail.com");
		var_dump($status);
	}

	public function test2() {
		$this->load->helper('custom_helper');
		$filter = ["id"=>1];
		$row = $this->Admin_model->fetch_tag_row('*','exam_settings',$filter);
		$exam_date = $row->exam_date;
		$time = $row->time;
		$room = $row->room;
		$issued_by = $row->issued_by;
		$next_year = date("Y") + 1;
		$this->load->view('test');
		$content = '<div style="background: #D0D7DF;width: 100%;padding: 30px;font-family: Arial">
			<h3>EXAMINATION PERMIT</h3>
			<h3 style="font-weight: normal;">Academic Year '.date("Y").' - '.$next_year.' </h3>
			<p>Status: Transferee</p>
			<p>Examinee: Transferee</p>
			<p>Exam Date: Transferee</p>
			<p>Time: Transferee</p>
			<p>Room: Transferee</p>
			<br>
			<p>Issued by: Transferee</p>
			<p>Date Issued: Transferee</p>
			<p>Reference number: Transferee</p>
			<p><b>INSTRUCTIONS:</b></p>
			<ul>
				<li>Examinees WITHOUT examination permit cannot take the test</li>
				<li>Please be at the examination site at least 15 minutes before the test</li>
				<li>Late examiness will not be allowed to take the test</li>
				<li>Bring 2 sharpened pencils with eraser</li>
				<li>High School Examiness should bring 1 (12 inches) ruler</li>
			</ul>
		</div>'	;
		$email = "jonasdulay28@gmail.com";
		$email_content = array("message"=>$content,"from"=>"letranadmission@gmail.com","from_name"=>"San Juan de Letran Calamba","to"=>$email,"subject"=>"Exam Permit - Letran Calamba");
		$status = sendMailCIMailer($email_content);
		//$this->load->view("templates/exam_success",$data)
		var_dump($status);
	}

	public function index()
	{
		if(!isset($this->session->ses_id)){
			$this->load->view('templates/admin_login_template');
		}else {
			$role = $this->session->ses_role;
			if($role != "Student") {
				redirect(base_url('Admin/dashboard'));
			}else{
				redirect(base_url());
			}
		}
	}

	public function send_confirmation() {
		$reference_number = clean_data(post('reference_number'));
		$filter = ["reference_number" => $reference_number];
		$row2 = $this->Admin_model->fetch_tag_row('*','application',$filter);
		$filter = ["id"=>1];
			$row = $this->Admin_model->fetch_tag_row('*','exam_settings',$filter);
			$exam_date = $row->exam_date;
			$time = $row->time;
			$room = $row->room;
			$issued_by = $row->issued_by;
			$next_year = date("Y") + 1;
			$transferee = (!empty($_FILES['report_card']['size']) ? 'Transferee' : 'New Student');
			$this->load->view('test');
			$content = '<div style="background: #D0D7DF;width: 100%;padding: 30px;font-family: Arial">
				<h3>EXAMINATION PERMIT</h3>
				<h3 style="font-weight: normal;">Academic Year '.date("Y").' - '.$next_year.' </h3>
				<p>Status: '.$transferee.'</p>
				<p>Examinee: '.$row2->last_name.', '.$row2->first_name.' '.$row2->middle_name.'</p>
				<p>Exam Date: '.$exam_date.'</p>
				<p>Time: '.$time.'</p>
				<p>Room: '.$room.'</p>
				<br>
				<p>Issued by: '.$issued_by.'</p>
				<p>Date Issued: '.date('m-d-Y').'</p>
				<p>Reference number: '.$reference_number.'</p>
				<p><b>INSTRUCTIONS:</b></p>
				<ul>
					<li>Examinees WITHOUT examination permit cannot take the test</li>
					<li>Please be at the examination site at least 15 minutes before the test</li>
					<li>Late examiness will not be allowed to take the test</li>
					<li>Bring 2 sharpened pencils with eraser</li>
					<li>High School Examiness should bring 1 (12 inches) ruler</li>
				</ul>
			</div>'	;
			$email = strtolower($row2->email);
			$email_content = array("message"=>$content,"from"=>"letranadmission@gmail.com","from_name"=>"San Juan de Letran Calamba","to"=>$email,"subject"=>"Exam Permit - Letran Calamba");
			$status = sendMailCIMailer($email_content);
	}

	function get_province_all() {
		$data["province"] = $this->Admin_model->fetch('refprovince','','','','provDesc ASC');
		echo json_encode($data);
	}

	function get_city_all() {
		$data["city"] = $this->Admin_model->fetch('refcitymun','','','','citymunDesc ASC');
		echo json_encode($data);
	}

	function get_brgy_all() {
		$data["brgy"] = $this->Admin_model->fetch('refbrgy','','','','brgyDesc ASC');
		echo json_encode($data);
	}

	public function login()
	{
		$response = ["message"=>"error"];
		$filter = [];
		foreach ($_POST as $key => $value) {
			$filter[$key] = clean_data($value);
		}
		//check if activated or not
		$filter["status"] = 1;
		//check if user exist
		$result = $this->Admin_model->fetch_tag_row("*","users",$filter);
		if(!empty($result)){
			if($result->role != "Student"){
				//set session
				$this->session->set_userdata('ses_id',encrypt($filter['reference_number']));
				$this->session->set_userdata('ses_role',encrypt($result->role));
				$response["message"] = "success";
			}
			
		}
		echo json_encode($response);
	}

	public function dashboard(){
		if(isset($this->session->ses_id) && isset($this->session->ses_role)){
				$this->load->view('templates/admin_template');	
		}else{
			redirect(base_url('admin'));
		}
		
	}

	public function logout() {
		session_destroy();
		$res["url"] = base_url('admin');
		echo json_encode($res);
	}


	public function register()
	{
		$data['content'] = 'pages/register';
		$this->load->view('templates/authentication_template',$data);
	}

	public function add()
	{

	}

	public function edit()
	{
		
	}

	public function delete()
	{
		
	}

	
}