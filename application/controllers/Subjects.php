<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subjects extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$this->load->model("Admin_model");
	}

	public function add()
	{
		$response["message"] = "error";
		$subject_name = clean_data(post('subject_name'));
		$data = ["subject_name"=>$subject_name];
		$check_exist = $this->Admin_model->fetch_tag_row("*","subjects",$data);
		if(empty($check_exist)){
			$this->Admin_model->insert('subjects',$data);
			$response["message"] = "success";
		}
		echo json_encode($response);
	}

	public function get_subjects() {
		$data['subjects'] = $this->Admin_model->fetch('subjects');
		echo json_encode($data);
	}

	public function edit()
	{
		
	}

	public function delete()
	{
		
	}

	
}