<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sample extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		if(isset($this->session->id))
		{
			$this->load->model("Crud_model");
		}
	}

	public function index()
	{
		$this->load->view('test');
	}

	public function add()
	{
		$config['upload_path'] = './uploads';
	    $config['allowed_types'] = 'jpg|png';
	    $config['max_size'] = '0';
	    $config['max_width'] = '0';
	    $config['max_height'] = '0';
	    $this->load->library('upload', $config);
	    // upload the image
	    if ($this->upload->do_upload('id_picture')) {
	        // success
	        // pass back $this->upload->data() for info
	    } else {
	        // failed
	        // pass back $this->upload->display_errors() for info
	        echo $this->upload->display_errors();
	    }
	}

	public function edit()
	{
		
	}

	public function delete()
	{
		
	}

	
}