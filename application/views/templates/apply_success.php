<!DOCTYPE html>
<html class="apply_page">
   <head>
      <!-- Site made with Mobirise Website Builder v4.9.2, https://mobirise.com -->
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="generator" content="Mobirise v4.9.2, mobirise.com">
      <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
      <link rel="shortcut icon" href="<?php echo base_url()?>assets/images/letranlogo.png" type="image/x-icon">
      <meta name="description" content="">
      <title>Letran Admission</title>
      <link rel="stylesheet" href="<?php echo base_url()?>assets/web/assets/mobirise-icons/mobirise-icons.css">
      <link rel="stylesheet" href="<?php echo base_url()?>assets/tether/tether.min.css">
      <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.min.css">
      <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap-grid.min.css">
      <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap-reboot.min.css">
      <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
      <link rel="stylesheet" href="<?php echo base_url()?>assets/css/main.css">
      <style type="text/css">
         input{
            text-transform: uppercase;
         }
      </style>
   </head>
   <body >
      <div class="container-fluid" style="background: #fff;">
         <div class="row">
            <div class="col-md-12">
               <img src="<?php echo images_bundle()?>logo.png" style="height: 80px;margin:0 auto;padding:10px;">
            </div>
         </div>
              
      </div>
      <div class="hero-image">
        <!-- <div class="hero-text">
          <h1>I am John Doe</h1>
          <p>And I'm a Photographer</p>
          <button>Hire me</button>
        </div> -->
      </div>
      <div class="container" style="margin-top:50px;">
         <div class="row">
            <div class="col-md-12">
               <div class="page-header">
                  <center><h1 class="text-primary">Account Information<br /></h1></center>
                  <div class="well">
                     <center><p><b>Please take note of your account</b></p></center>
                     <label><b>Reference number: </b></label>
                     <p><?php echo $this->session->flashdata('reference_number');?></p>
                     <label><b>Password: </b></label>
                     <p><?php echo $this->session->flashdata('password');?></p>
                     <center><a href="<?php echo base_url('Admission')?>"><button class="btn btn-success">Login</button></a></center>
                  </div>
               </div>
            </div>
            <div class="col-md-12">
            </div>
         </div>
      </div>
      <!--footer starts from here-->
      <footer class="footer">
      <div class="container bottom_border">
      <div class="row">
      <div class=" col-sm-4 col-md col-sm-4  col-12 col">
      <h5 class="headin5_amrc col_white_amrc pt2">Find us</h5>
      <!--headin5_amrc-->
      <p class="mb10">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
      <p><i class="fa fa-location-arrow"></i> 9878/25 sec 9 rohini 35 </p>
      <p><i class="fa fa-phone"></i>  +91-9999878398  </p>
      <p><i class="fa fa fa-envelope"></i> info@example.com  </p>


      </div>


      <div class=" col-sm-4 col-md  col-6 col">
      <h5 class="headin5_amrc col_white_amrc pt2">Quick links</h5>
      <!--headin5_amrc-->
      <ul class="footer_ul_amrc">
      <li><a href="">Home</a></li>
      <li><a href="">About</a></li>
      <li><a href="">Academics</a></li>
      <li><a href="">Admission</a></li>
      <li><a href="">Departmen</a></li>
      <li><a href="">Contact</a></li>
      </ul>
      <!--footer_ul_amrc ends here-->
      </div>


      <div class=" col-sm-4 col-md  col-6 col">
      <h5 class="headin5_amrc col_white_amrc pt2">Quick links</h5>
      <!--headin5_amrc-->
      <ul class="footer_ul_amrc">
      <li><a href="">Home</a></li>
      <li><a href="">About</a></li>
      <li><a href="">Academics</a></li>
      <li><a href="">Admission</a></li>
      <li><a href="">Departmen</a></li>
      <li><a href="">Contact</a></li>
      </ul>
      <!--footer_ul_amrc ends here-->
      </div>


      <div class=" col-sm-4 col-md  col-12 col">
      <h5 class="headin5_amrc col_white_amrc pt2">Follow us</h5>
      <!--headin5_amrc ends here-->

      <ul class="footer_ul2_amrc">
      <li><a href="#"><i class="fab fa-twitter fleft padding-right"></i> </a><p>Lorem Ipsum is simply dummy text of the printing...<a href="#">https://www.lipsum.com/</a></p></li>
      <li><a href="#"><i class="fab fa-twitter fleft padding-right"></i> </a><p>Lorem Ipsum is simply dummy text of the printing...<a href="#">https://www.lipsum.com/</a></p></li>
      <li><a href="#"><i class="fab fa-twitter fleft padding-right"></i> </a><p>Lorem Ipsum is simply dummy text of the printing...<a href="#">https://www.lipsum.com/</a></p></li>
      </ul>
      <!--footer_ul2_amrc ends here-->
      </div>
      </div>
      </div>


      <div class="container">
      <ul class="foote_bottom_ul_amrc">
      <li><a href="">Home</a></li>
      <li><a href="">About</a></li>
      <li><a href="">Academics</a></li>
      <li><a href="">Admission</a></li>
      <li><a href="">Departmen</a></li>
      <li><a href="">Contact</a></li>
      </ul>
      <!--foote_bottom_ul_amrc ends here-->
      <p class="text-center">Copyright @2019 | Copyright <a href="#">Colegio de San Juan Letran Calamba</a></p>
      <!--social_footer_ul ends here-->
      </div>

      </footer>

      <script src="<?php echo base_url()?>assets/web/assets/jquery/jquery.min.js"></script>
      <script src="<?php echo base_url()?>assets/popper/popper.min.js"></script>
      <script src="<?php echo base_url()?>assets/tether/tether.min.js"></script>
      <script src="<?php echo base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
      <script src="<?php echo base_url()?>assets/dropdown/js/script.min.js"></script>
      <script src="<?php echo base_url()?>assets/touchswipe/jquery.touch-swipe.min.js"></script>
      <script src="<?php echo base_url()?>assets/smoothscroll/smooth-scroll.js"></script>
      <script src="<?php echo base_url()?>assets/theme/js/script.js"></script>
   </body>
</html>