<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Letran Admission</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo admin_folder();?>bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo admin_folder();?>bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo admin_folder();?>bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="shortcut icon" href="<?php echo base_url()?>assets/images/letranlogo.png" type="image/x-icon">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->

  <link rel="stylesheet" href="<?php echo admin_folder();?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo admin_folder();?>dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo styles_bundle()?>ladda.min.css">
  <link rel="stylesheet" href="<?php echo styles_bundle()?>sweetalert2.min.css">
    <link rel="stylesheet" href="<?php echo admin_folder();?>dist/css/skins/skin-blue.min.css">
    <link rel="stylesheet" href="<?php echo styles_bundle()?>bootstrap-select.min.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
  <style type="text/css">
  	html, body, .wrapper{
  		    height: 100% !important;
  	}
  	.glyphicon {
  		top: -1px !important;
    	left: 2px !important;
  	}/*
  	.bootstrap-select.btn-group .dropdown-menu li a span.check-mark{
  		display: inline-block !important; 
  	}*/
  </style>
  <script type="text/javascript">
  	var base_url = "<?php echo base_url()?>";
  	var role = "<?php echo decrypt($this->session->ses_role); ?>"
  </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div id="app"></div>
<!-- jQuery 3 -->
<script type="text/javascript">
	function labelFormatter(label, series) {
    return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
      + label
      + '<br>'
      + Math.round(series.percent) + '%</div>'
  }
</script>
<script src="<?php echo admin_folder();?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo admin_folder();?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo admin_folder();?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo admin_folder();?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script><!-- 
<script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script> -->
<script src="<?php echo scripts_bundle()?>spin.min.js"></script>
<script src="<?php echo scripts_bundle()?>ladda.min.js"></script>
<script src="<?php echo scripts_bundle()?>bootstrap-select.min.js"></script>
<!-- FastClick -->
<script src="<?php echo admin_folder();?>bower_components/fastclick/lib/fastclick.js"></script>

<!-- AdminLTE App -->
<script src="<?php echo admin_folder();?>dist/js/adminlte.min.js"></script>
<script src="<?php echo scripts_bundle()?>sweetalert2.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo admin_folder();?>dist/js/demo.js"></script>
<script src="<?php echo admin_folder();?>bower_components/Flot/jquery.flot.js"></script>
<!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
<script src="<?php echo admin_folder();?>bower_components/Flot/jquery.flot.resize.js"></script>
<!-- FLOT PIE PLUGIN - also used to draw donut charts -->
<script src="<?php echo admin_folder();?>bower_components/Flot/jquery.flot.pie.js"></script>
<!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
<script src="<?php echo admin_folder();?>bower_components/Flot/jquery.flot.categories.js"></script>
<script type="text/javascript" src="<?php echo scripts_bundle()?>global.js"></script>
<script src="<?php echo base_url()?>dist/assets/src/js/admin.js"></script>
</body>
</html>
