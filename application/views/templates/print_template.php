<!DOCTYPE html>
<html class="admin_login_page">
<head>
  <!-- Site made with Mobirise Website Builder v4.9.2, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.9.2, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="<?php echo base_url()?>assets/images/letranlogo.png" type="image/x-icon">
  <meta name="description" content="">
  <title>Letran Admission</title>
  <link rel="stylesheet" href="<?php echo base_url()?>assets/web/assets/mobirise-icons/mobirise-icons.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/tether/tether.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap-reboot.min.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
<script type="text/javascript">
	window.print();
</script>
<style type="text/css">
	@page{
		margin-top: 50px;
	}
</style>
</head>
<body >
	<?php 
		$from = get('from');
		$to = get('to'); 
	?>
	<br>
	<center><h2>Data from <?php echo $from.' to '.$to;?></h2></center>
	<table class="table table-striped table-bordered" style="width:100%;">
    <thead>
      <tr>
        <th>City</th>
        <th>Count</th>
      </tr>
    </thead>
    <tbody>
    	<?php foreach($reports as $key ){ ?>
      <tr>
        <td><?php echo $key->label?></td>
        <td><?php echo $key->data?></td>
      </tr>
     <?php } ?>
    </tbody>
  </table>
</body>
</html>