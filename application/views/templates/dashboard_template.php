<!DOCTYPE html>
<html class="dashboard_page">
<head>
  <!-- Site made with Mobirise Website Builder v4.9.2, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.9.2, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="<?php echo base_url()?>assets/images/letranlogo.png" type="image/x-icon">
  <meta name="description" content="">
  <title>Letran Admission</title>
  <link rel="stylesheet" href="<?php echo base_url()?>assets/web/assets/mobirise-icons/mobirise-icons.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/tether/tether.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="<?php echo styles_bundle()?>sweetalert2.min.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/main.css">
  <style type="text/css">
  	.box:hover {
  		background: #004EA8;
  		-webkit-transition:  .5s; /* Safari */
  		transition:  .5s;
  	}
  	.box:hover span	{
  		color:#fff !important;
  	}
    .modal-dialog {
  position: absolute;
  top: 40%;
  left: 50%;
  transform: translate(-50%, -50%) !important;
}
  </style>
</head>
<body>
	<div  class="container-fluid" style="background: #fff;padding:10px 20px;    box-shadow: 5px 2px 2px 0px rgba(0,0,0,0.75);">
		<div class="row">
			<div class="col-md-12">
				<center>
				<img src="<?php echo images_bundle()?>logo.png" class="img-fluid"  style="height: 70px;">
				</center>
			</div>
		</div>
	</div>
  <div class="dashboard_banner" style="width: 100%;min-height: 150px;background:#004EA8 ">  

  </div>
  <div class="container" style="min-height: 500px;margin-top: 50px;">
    <div class="row"> 
        <div class="col-md-4">
          <a href="#" id="permit_box">
          	<div class="box">  
              <center>  <span class="mbri-edit" style="font-size: 120px; color:#2176d8;"></span></center>
          	</div>
          </a>
          <br>
          <h5 class="text-center ">Take Online Exam</h5> 
        </div>
        <div class="col-md-4">
          <a href="#" data-toggle="modal" data-target="#reupload_modal">
          	<div class="box">  
              <center>  <span class="mbri-upload" style="font-size: 120px; color:#2176d8;"></span></center>
          	</div>
          </a>
          <br>
          <h5 class="text-center ">Re-upload Documents</h5> 
        </div>
        <div class="col-md-4">
          <a href="#" data-toggle="modal" data-target="#feedback_modal">
          	<div class="box">  
	              <center>  <span class="mbri-cust-feedback" style="font-size: 120px; color:#2176d8;"></span></center>
	          </div>
          </a>
          <br>
          <h5 class="text-center ">Feedback</h5> 
        </div>
        <br>
        <div class="col-md-4">
          <a href="<?php echo base_url('Dashboard/logout')?>">
          	<div class="box">  
	              <center>  <span class="mbri-logout" style="font-size: 120px; color:#2176d8;"></span></center>
	          </div>
          </a>
          <br>
          <h5 class="text-center ">Logout</h5> 
        </div>
    </div>  
  </div>
  <?php $this->load->view('includes/footer') ?>
  <!-- Modal -->
  <div class="modal fade" id="reupload_modal" tabindex="-1" role="dialog" aria-labelledby="feedback_modalTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="feedback_modalTitle">Re-upload Documents</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <?php echo form_open_multipart("Dashboard/reupload_documents","method='POST' id='reupload_documents'");?>
        <div class="modal-body">
          <div class="form-group">
	            <label for="FormerSchoolAddress">1x1 recent ID picture (png or jpg)</label><br>
	            <input   id="" name="id_picture" type="file" value="" accept="image/*"  />
	         </div>
	         <div class="form-group">
	            <label for="FormerSchoolAddress">Photocopy of NSO/PSA Authenticated Birth Certificate (pdf format)</label><br>
	            <input id="" name="nso_psa" type="file" value="" accept="application/pdf"  />
	         </div>
	         <div class="form-group">
	            <label for="FormerSchoolAddress">Certificate of Candidacy for Promotion of Graduation (pdf format) <small><span>(for new student only)</span></small></label><br>
	            <input  id="" name="certificate_of_candidacy" type="file" value="" accept="application/pdf"  />
	         </div>
	         <div class="form-group">
	            <label for="FormerSchoolAddress">Photocopy of Report Card (Form 138) (pdf format) <small><span>(for transferee only)</span></small></label><br>
	            <input id="" name="report_card" type="file" value=""  accept="application/pdf"   />
	         </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        <?php echo form_close()?>
      </div>
    </div>
  </div>

  <div class="modal fade" id="feedback_modal" tabindex="-1" role="dialog" aria-labelledby="feedback_modalTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="feedback_modalTitle">Student Feedback</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <?php echo form_open("#","method='POST' id='feedback_form'");?>
        <div class="modal-body">
          <textarea style="resize: vertical;min-height: 400px;" class="form-control" name="feedback" placeholder="Please describe your feedback here" required></textarea>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Send</button>
        </div>
        <?php echo form_close()?>
      </div>
    </div>
  </div>

  <div class="modal fade" id="permit_modal" tabindex="-1" role="dialog" aria-labelledby="permit_modal" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="permit_modal">Please enter your permit key</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <?php echo form_open("#","method='POST' id='feedback_form'");?>
        <div class="modal-body">
          <input type="text" name="permit_key" class="form-control" placeholder="Permit key">
          <br>
          <center><button type="submit" class="btn btn-primary">Access</button></center>
        </div>
        <?php echo form_close()?>
      </div>
    </div>
  </div>

  <script src="<?php echo base_url()?>assets/web/assets/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url()?>assets/popper/popper.min.js"></script>
  <script src="<?php echo base_url()?>assets/tether/tether.min.js"></script>
  <script src="<?php echo base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?php echo scripts_bundle()?>sweetalert2.min.js"></script>
  <script src="<?php echo scripts_bundle()?>global.js"></script>

  <script>
    var base_url = "<?php echo base_url()?>";
    $("#feedback_form").on("submit",function(e){
        e.preventDefault();
        var datastring = $("#feedback_form").serialize();
        $.ajax({
            type: "POST",
            url: base_url+"Dashboard/add_feedback",
            data: datastring,
            dataType: "json",
            success: function(data) {
              $('#feedback_modal').modal('toggle'); 
                swal("Success", "Feedback was sent successfully", "success")
            },
            error: function(err) {
                console.log(err);
            }
        });
    })
    $("#permit_box").on("click",function(e){
      swal({
        title: 'Enter Permit Key',
        input: 'text',
        inputAttributes: {
          autocapitalize: 'off'
        },
        showCancelButton: false,
        confirmButtonText: 'Access',
        preConfirm: (permit_key) => {
          $.ajax({
              type: "POST",
              url: base_url+"Dashboard/validate_key",
              data: {"permit_key":permit_key,token: getCookie('csrf_token')},
              dataType: "json",
              success: function(response) {
                console.log(response)
                if(response.message == "success"){
                  location.href="Exams"
                }else {
                  swal.showValidationError(
                    'Invalid permit key'
                  );
                  swal.enableConfirmButton()
                }
              },
              error: function(err) {
                  console.log(err);
              }
          });
          
        },
        allowOutsideClick: false
      })
    })
    
  </script>
</body>
</html>