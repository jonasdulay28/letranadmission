<!DOCTYPE html>
<html class="admin_login_page">
<head>
  <!-- Site made with Mobirise Website Builder v4.9.2, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.9.2, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="<?php echo base_url()?>assets/images/letranlogo.png" type="image/x-icon">
  <meta name="description" content="">
  <title>Letran Admission</title>
  <link rel="stylesheet" href="<?php echo base_url()?>assets/web/assets/mobirise-icons/mobirise-icons.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/tether/tether.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap-reboot.min.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo styles_bundle()?>sweetalert2.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/main.css">
  <style type="text/css">
  	.app {
  		  display: -ms-flexbox;
		    display: flex;
		    -ms-flex-direction: column;
		    flex-direction: column;
		    min-height: 100vh;
        background-color: #e4e5e6;
  	}
    .swal2-title{
      font-family: 'Roboto' !important;
    }
  </style>
  
</head>
<body class="app flex-row align-items-center">
	<div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="card-group">
            <div class="card p-4">
              <div class="card-body">
                <h1>Login</h1>
                <?php echo form_open("Admin/login",array('method'=>'POST','autocomplete'=>'off','id'=>'login_form')); ?>
                <p class="text-muted">Sign In to your account</p>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                     <span class="mbri-user"></span>
                    </span>
                  </div>
                  <input class="form-control" type="text" placeholder="Username" name="reference_number">
                </div>
                <div class="input-group mb-4">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <span class="mbri-lock"></span>
                    </span>
                  </div>
                  <input class="form-control" type="password" placeholder="Password" name="password">
                </div>
                <div class="row">
                  <div class="col-6">
                    <button class="btn btn-primary px-4" type="submit">Login</button>
                  </div>
                </div>
              </div>
              <?php echo form_close()?>
            </div>
            <div class="card text-white bg-primary py-5 d-md-down-none" style="width:44%;background: #f7f7f7  !important;">
              <div class="card-body text-center">
                <div>
                  <img src="<?php echo images_bundle()?>admission_logo.png" class="img-fluid" style="height: 160px;">
                  <h2>Colegio de San Juan de Letran Calamba</h2>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <script src="<?php echo base_url()?>assets/web/assets/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url()?>assets/popper/popper.min.js"></script>
  <script src="<?php echo base_url()?>assets/tether/tether.min.js"></script>
  <script src="<?php echo base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url()?>assets/dropdown/js/script.min.js"></script>
  <script src="<?php echo base_url()?>assets/touchswipe/jquery.touch-swipe.min.js"></script>
  <script src="<?php echo base_url()?>assets/smoothscroll/smooth-scroll.js"></script>
  <script src="<?php echo scripts_bundle()?>sweetalert2.min.js"></script>
  <script src="<?php echo base_url()?>assets/theme/js/script.js"></script>
  <script type="text/javascript">
    var base_url = "<?php echo base_url()?>";
    $("#login_form").on("submit",function(e){
        e.preventDefault();
        var datastring = $("#login_form").serialize();
        $.ajax({
            type: "POST",
            url: base_url+"Admin/login",
            data: datastring,
            dataType: "json",
            success: function(data) {
                if(data.message == "success") {
                    window.location.href = base_url + "admin/Dashboard"
                } else {
                    swal("Error", "Invalid reference number or password", "error")
                }

            },
            error: function(err) {
                console.log(err);
            }
        });
    })
  </script>
  
</body>
</html>