<!DOCTYPE html>
<html class="apply_page">
   <head>
      <!-- Site made with Mobirise Website Builder v4.9.2, https://mobirise.com -->
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="generator" content="Mobirise v4.9.2, mobirise.com">
      <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
      <link rel="shortcut icon" href="<?php echo base_url()?>assets/images/letranlogo.png" type="image/x-icon">
      <meta name="description" content="">
      <title>Letran Admission</title>
      <link rel="stylesheet" href="<?php echo base_url()?>assets/web/assets/mobirise-icons/mobirise-icons.css">
      <link rel="stylesheet" href="<?php echo base_url()?>assets/tether/tether.min.css">
      <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.min.css">
      <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap-grid.min.css">
      <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap-reboot.min.css">
      <link rel="stylesheet" href="<?php echo styles_bundle()?>sweetalert2.min.css">
      <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
      <link rel="stylesheet" href="<?php echo base_url()?>assets/css/main.css">
      <style type="text/css">
         input{
            text-transform: uppercase;
         }
      </style>
   </head>
   <body >
      <div class="container-fluid" style="background: #fff;">
         <div class="row">
            <div class="col-md-12">
               <img src="<?php echo images_bundle()?>logo.png" style="height: 80px;margin:0 auto;padding:10px;">
            </div>
         </div>
              
      </div>
      <div class="hero-image">
        <!-- <div class="hero-text">
          <h1>I am John Doe</h1>
          <p>And I'm a Photographer</p>
          <button>Hire me</button>
        </div> -->
      </div>
      <div class="container" style="margin-top:50px;">
         <div class="row">
            <div class="col-md-12">
               <div class="page-header">
                  <center><h1 class="text-primary">Online Student Application<br /></h1></center>
               </div>
            </div>
            <div class="col-md-12">
               <?php echo form_open_multipart("Admission/apply",array('method'=>'POST','autocomplete'=>'off','id'=>'application_form')); ?>
                  <div class="validation-summary-valid" data-valmsg-summary="true">
                     <ul>
                        <li style="display:none"></li>
                     </ul>
                  </div>
                  <div class="form-group">
                     <!-- <div class="row well">
                        <div class="col-md-12">
                           <div class="form-group">
                              <input id="TypeOfApplication" name="TypeOfApplication" type="hidden" value="" />
                              <span class="field-validation-valid text-danger" data-valmsg-for="TypeOfApplication" data-valmsg-replace="true"></span>
                              <h4 class="text-primary">Basic Education</h4>
                           </div>
                           <div class="form-group">
                              <label for="YearEntry">School Year</label>
                              <select class="form-control" data-val="true" data-val-number="The field Entry Year must be a number." data-val-required="The Entry Year field is required." id="YearEntry" name="YearEntry">
                                 <option value="">Select year of entry</option>
                                 <option value="343">Regular Period, 2019-2020</option>
                              </select>
                              <span class="field-validation-valid text-danger" data-valmsg-for="YearEntry" data-valmsg-replace="true"></span>
                           </div>
                           <div class="form-group">
                              <div class="col-md-12">
                                 <input data-val="true" data-val-number="The field EducationLevel must be a number." id="EducationLevel" name="EducationLevel" type="hidden" value="" />
                              </div>
                           </div>
                           <div class="form-group">
                              <label>Grade Level</label>
                              <select class="form-control" data-val="true" data-val-required="The GradeLevel field is required." id="GradeLevel" name="GradeLevel">
                                 <option value="">Select grade level</option>
                                 <option value="7">GRADE 7</option>
                              </select>
                              <span class="field-validation-valid text-danger" data-valmsg-for="GradeLevel" data-valmsg-replace="true"></span>
                           </div>
                        </div>
                     </div> -->
                     <div class="row well">
                        <div class="col-md-12">
                           <div class="form-group">
                              <h4 class="text-primary">Personal Information</h4>
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="form-group">
                              <label for="LastName">Last Name <span style="color:red">*</span></label>
                              <input class="form-control" data-val="true" id="last_name" name="last_name" type="text" value="" required  />
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="form-group">
                              <label for="FirstName">Given Name <span style="color:red">*</span></label>
                              <input class="form-control" id="first_name" name="first_name" type="text" value="" required />
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group">
                              <label for="MiddleName">Middle Name</label>
                              <input class="form-control"id="middle_name" name="middle_name" type="text" value=""  />
                           </div>
                        </div>
                        <div class="col-md-1">
                           <div class="form-group">
                              <label for="MiddleName">Suffix</label>
                              <input class="form-control" type="text" value="" name="suffix" />
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="Address">House No./Street</label>
                              <input class="form-control"  id="address" name="address" type="text" value="" />
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="Address">Subdivision</label>
                              <input class="form-control" id="subdivision" name="subdivision" type="text" value="" />
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group">
                              <label for="City">Province <span style="color:red">*</span></label>
                              <select class="form-control" name="province"  id="province" required="">
                              	<option value=""> Please select your province</option>
                              	<?php foreach ($province as $key) { ?>
                              		<option value="<?php echo $key->provCode ?>"><?php echo $key->provDesc ?></option>
                              	<?php } ?>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group">
                              <label for="City">City <span style="color:red">*</span></label>
                              <select class="form-control" name="city" id="city" required="">
                              	<option value=""> Please select your city</option>
                              	<?php foreach ($city as $key) { ?>
                              		<option value="<?php echo $key->citymunCode ?>" data-chained="<?php echo $key->provCode?>"><?php echo $key->citymunDesc ?></option>
                              	<?php } ?>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-3">
                           <div class="form-group">
                              <label for="Province">Barangay/Town</label>
                              <select class="form-control" name="town" id="town" disabled=""  required="">
                              	<option value=""> Please select your barangay/town</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-2">
                           <div class="form-group">
                              <label for="City">Postal Code</label>
                              <input class="form-control" type="number" name="postal_code" value="" />
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="Birthday">Birthday <span style="color:red">*</span></label>
                              <input class="form-control" id="Birthday" name="birthday" type="date" value="" required />
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="BirthPlace">Birth Place <span style="color:red">*</span></label>
                              <input class="form-control" id="BirthPlace" name="birth_place" type="text" value="" required />
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="LandlineNo">Landline No</label>
                              <input class="form-control" id="LandlineNo" name="landline" type="number" value="" />
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="MobileNo">Mobile No <span style="color:red">*</span></label>
                              <input class="form-control" id="MobileNo" name="mobile" type="number" value="" maxlength="11" required />
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="form-group">
                              <label for="Citizenship">Citizenship <span style="color:red">*</span></label>
                              <input class="form-control" id="Citizenship" name="citizenship" type="text" value="" required />
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="form-group">
                              <label for="CivilStatus">Civil Status</label>
                              <select class="form-control" id="statustype" name="civil_status" >
                                 <option value="SINGLE">Single</option>
                                 <option value="SEPERATED">Seperated</option>
                                 <option value="MARRIED">Married</option>
                                 <option value="DIVORCED">Divorced</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="form-group">
                              <label for="Religion">Religion</label>
                              <input class="form-control" id="Religion" name="religion" type="text" value="" />
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="EmailAddress">Email Address <span style="color:red">*</span></label>
                              <input class="form-control"  id="EmailAddress" name="email" type="email" value="" required/>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="Gender">Gender</label>
                              <select class="form-control" id="Gender" name="gender">
                                 <option value="M">Male</option>
                                 <option value="F">Female</option>
                              </select>
                           </div>
                        </div>
                     </div>
                     <div class="row well">
                        <div class="col-md-6">
                           <div class="form-group">
                              <h4 class="text-primary">Father's Information</h4>
                           </div>
                           <div class="form-group">
                              <label for="FatherName">Father&#39;s Name</label>
                              <input class="form-control"  id="FatherName" name="father_name" type="text" value="" />
                           </div>
                           <div class="form-group">
                              <label for="FatherContact">Father&#39;s Contact No</label>
                              <input class="form-control"  id="FatherContact" name="father_contact" type="number" value="" />
                           </div>
                           <div class="form-group">
                              <label for="FatherEmail">Father&#39;s Email</label>
                              <input class="form-control" id="FatherEmail" name="father_email" type="email" value="" />
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <h4 class="text-primary">Mother's Information</h4>
                           </div>
                           <div class="form-group">
                              <label for="MotherName">Mother&#39;s Name</label>
                              <input class="form-control" id="MotherName" name="mother_name" type="text" value="" />
                           </div>
                           <div class="form-group">
                              <label for="MotherContact">Mother&#39;s Contact No</label>
                              <input class="form-control" id="MotherContact" name="mother_contact" type="number" value="" />
                           </div>
                           <div class="form-group">
                              <label for="MotherEmail">Mother&#39;s Email</label>
                              <input class="form-control " id="MotherEmail" name="mother_email" type="email" value="" />
                           </div>
                        </div>
                        <div class="col-md-12">
                           <div class="form-group">
                              <label for="MotherName">Number of Siblings</label>
                              <input class="form-control" data-val="true" id="MotherName" name="no_sibling" type="number" value="" />
                           </div>
                        </div>
                        <div class="col-md-12">
                           <div class="form-group">
                              <h4 class="text-primary">Guardian's Information</h4>
                           </div>
                           <div class="form-group">
                              <label for="GuardianName">Guardian&#39;s Name</label>
                              <input class="form-control" id="GuardianName" name="guardian_name" type="text" value="" />
                           </div>
                           <div class="form-group">
                              <label for="GuardianAddress">Guardian&#39;s Address</label>
                              <input class="form-control"id="GuardianAddress" name="guardian_address" type="text" value="" />
                           </div>
                           <div class="form-group">
                              <label>Relationship to Guardian</label>
                              <input class="form-control" id="RelationshipToGuardian" name="guardian_relationship" type="text" value="" />
                           </div>
                           <div class="form-group">
                              <label for="GuardianContactNo">Guardian Contact No</label>
                              <input class="form-control" id="GuardianContactNo" name="guardian_contact" type="number" value="" />
                           </div>
                        </div>
                     </div>
                     <div class="row well">
                        <div class="col-md-12">
                           <div class="form-group">
                              <h4 class="text-primary">Educational Data</h4>
                           </div>
                           <div class="form-group">
                              <label for="FormerSchool">School Last Attended</label>
                              <input class="form-control"  id="FormerSchool" name="former_school" type="text" value="" />
                           </div>
                           <div class="row">
                              <div class="col-md-6">
                              <div class="form-group">
                                 <label for="Former_School_Type">Program (Transferee only):</label>
                                 <input class="form-control" id="FormerSchool" name="program" type="text" value="" />
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="Former_School_Type">School Year</label>
                                 <input class="form-control"id="FormerSchool" name="school_year" type="text" value="" />
                              </div>
                           </div>
                           </div>
                           <div class="form-group">
                              <label for="FormerSchoolAddress">School Address</label>
                              <input class="form-control"  id="FormerSchoolAddress" name="school_address" type="text" value="" />
                           </div>
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <label for="GPA">General Weighted Average 1st sem</label>
                                    <input class="form-control"  id="GPA" name="gwa_first" type="number" value="" />
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <label for="GPA">General Weighted Average 2nd sem</label>
                                    <input class="form-control" id="GPA" name="gwa_second" type="number" value="" />
                                 </div>
                              </div>
                           </div>
                           <label for="GPA">Senior High School Track (To be completed by college freshmen applicant):</label>
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <select class="form-control" name="track_type">
                                       <option value="ACADEMIC">Academic</option>
                                       <option value="ARTS AND DESIGN">Arts and Design</option>
                                       <option value="SPORTS">Sports</option>
                                       <option value="TECHNOLOGY-VOCATIONAL-LIVELIHOOD">Technology-Vocational-Livelihood</option>
                                    </select>   
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <select class="form-control" name="track_sub_type">
                                       <option value="STEM">STEM</option>
                                       <option value="ABM">ABM</option>
                                       <option value="HUMSS">HUMSS</option>
                                       <option value="GA">GA</option>
                                       <option value="HOME ECONOMICS">Home Economics</option>
                                       <option value="AGRI-FISHERY">Agri-Fishery</option>
                                       <option value="INDUSTRIAL ARTS">Industrial Arts</option>
                                       <option value="INFORMATION AND COMMUNICATIONS TECHNOLOGY">Information and Communications Technology</option>
                                    </select>   
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="HonorsReceived">Honors Received</label>
                              <input class="form-control" id="HonorsReceived" name="honors" type="text" value="" />
                           </div>
                        </div>
                     </div>
                     <div class="row well">
                        <div class="col-md-12">
                           <div class="form-group">
                              <h4 class="text-primary">Reasons for applying at Letran Calamba?</h4>
                           </div>
                           <div class="form-group">
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="checkbox">
                                       <label><input  name="apply_reason[]" type="checkbox" value="QUALITY EDUCATION" />Quality Education</label>
                                    </div>
                                    <div class="checkbox">
                                       <label><input  name="apply_reason[]" type="checkbox" value="AFFORDABILITY" />Affordability</label>
                                    </div>
                                    <div class="checkbox">
                                       <label><input  name="apply_reason[]" type="checkbox" value="GOOD FACILITIES" />Good Facilities</label>
                                    </div>
                                    <div class="checkbox">
                                       <label><input  name="apply_reason[]" type="checkbox" value="PROXIMITY" />Proximity</label>
                                    </div>
                                    <div class="checkbox">
                                       <label><input  name="apply_reason[]" type="checkbox" value="AVAILABILITY OF PROGRAM" />Availability of Program</label>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="checkbox">
                                       <label><input  name="apply_reason[]" type="checkbox" value="RECOMMENDED BY SCHOOL" />Recommended by School</label>
                                    </div>
                                    <div class="checkbox">
                                       <label><input  name="apply_reason[]" type="checkbox" value="COMPETENT PROFESSORS" />Competent Professors</label>
                                    </div>
                                    <div class="checkbox">
                                       <label><input  name="apply_reason[]" type="checkbox" value="CAMPUS ENVIRONMENT" />Campus Environment</label>
                                    </div>
                                    <div class="checkbox">
                                       <label><input  name="apply_reason[]" type="checkbox" value="ALUMNUS" />Alumnus</label>
                                    </div>
                                 </div>
                              </div>
                              <div class="checkbox">
                                 <label>Others</label>
                              </div>
                              <input class="form-control" id="Q7" name="apply_others" style="font-size:16px;" type="text" value="" />                            
                           </div>
                        </div>
                     </div>
                     <div class="row well">
                        <div class="col-md-12">
                           <div class="form-group">
                              <h4 class="text-primary">How did you come to know about Letran Calamba? Through</h4>
                           </div>
                           <div class="form-group">
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="checkbox">
                                       <label><input  name="referal[]" type="checkbox" value="STUDENTS OF LETRAN" />Students of Letran</label>
                                    </div>
                                    <div class="checkbox">
                                       <label><input  name="referal[]" type="checkbox" value="EMPLOYEES OF LETRAN" />Employees of Letran</label>
                                    </div>
                                    <div class="checkbox">
                                       <label><input  name="referal[]" type="checkbox" value="FACULTY OF LETRAN" />Faculty of Letran</label>
                                    </div>
                                    <div class="checkbox">
                                       <label><input  name="referal[]" type="checkbox" value="NEWSPAPER ADS" />Newspaper Ads</label>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                     <div class="checkbox">
                                       <label><input  name="referal[]" type="checkbox" value="BILLBOARDS" />Billboards</label>
                                    </div>
                                    <div class="checkbox">
                                       <label><input  name="referal[]" type="checkbox" value="STREAMERS" />Streamers</label>
                                    </div>
                                    <div class="checkbox">
                                       <label><input  name="referal[]" type="checkbox" value="CAREER ORIENTATION / TALK" />Career Orientation / Talk</label>
                                    </div>
                                 </div>
                              </div>
                              <div class="checkbox">
                                 <label>Others</label>
                              </div>
                              <input class="form-control" id="Q7" name="referal_others" style="font-size:16px;" type="text" value="" />                            
                           </div>
                        </div>
                     </div>
                     <div class="row well">
                        <div class="col-md-12">
                           <div class="form-group">
                              <h4 class="text-primary">Documents</h4>
                           </div>
                           <div class="form-group">
                              <label for="FormerSchoolAddress">1x1 recent ID picture (png or jpg)<span style="color:red">*</span></label><br>
                              <input   id="" name="id_picture" type="file" value="" accept="image/*" required />
                           </div>
                           <div class="form-group">
                              <label for="FormerSchoolAddress">Photocopy of NSO/PSA Authenticated Birth Certificate (pdf format) <span style="color:red">*</span></label><br>
                              <input id="" name="nso_psa" type="file" value="" accept="application/pdf"  required/>
                           </div>
                           <div class="form-group">
                              <label for="FormerSchoolAddress">Certificate of Candidacy for Promotion of Graduation (pdf format) <small><span>(for new student only)</span></small></label><br>
                              <input  id="" name="certificate_of_candidacy" type="file" value="" accept="application/pdf"  />
                           </div>
                           <div class="form-group">
                              <label for="FormerSchoolAddress">Photocopy of Report Card (Form 138) (pdf format) <small><span>(for transferee only)</span></small></label><br>
                              <input id="" name="report_card" type="file" value=""  accept="application/pdf"   />
                           </div>
                        </div>
                     </div>
                     <div class="row well">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="form-group">
                                 <h4 class="text-primary">Certification</h4>
                              </div>
                           </div>
                           <div class="form-group">
                           		<p>
                           			<b>I acknowledge that:</b>
																<li>I have not been involved in any disciplinary case; and </li>
																<li>The Admissions, Student Assistance and Scholarships Office (AdSAScO) has the right to terminate my application and enrollment for non-compliance in the admission and enrollment process.</li>
																<br>
																<b>By signifying my consent, I agree with the following:</b>
																<li>Republic Act No. 10173, or the Data Privacy Act of 2012, and other relevant Philippine laws apply to the collection and processing of the student’s personal data.</li>
																<li>By applying for admission/registering as a student, I am allowing the Colegio to collect, use, and process the student’s personal data, when in the Colegio’s determination, a </li>legitimate educational or institutional interest exists, as enumerated in the Policy and other like circumstances.</li>
																<li>I am confirming that the personal information to the Colegio is true and correct. I understand that the Colegio reserves the right to revise any decision made based on the information I provide should the information be found to be untrue or incorrect.</li>
																<li>By entering into this Agreement, I am not relying upon any oral or written representations or statements made by the Colegio other than what is set forth in this Agreement.</li>
																<li>My consent to the Policy and the Agreement is among the conditions to a student’s admission into the Colegio.</li>
																<li>Any issue that may arise regarding the processing of my personal information will be settled amicably with the Colegio before resorting to the appropriate arbitration or court proceedings within Philippine jurisdiction.</li>
																<li>This authorization and consent will continue to have effect throughout the duration of the student’s stay with the Colegio and the period set until destruction or disposal of records, unless withdrawn in writing.</li>
																<br>
																<b>Please take the time to read the Policy and its terms and thereafter check the appropriate statement:</b>
                           		</p>
                              <p>
                                 <input data-val="true"  name="Certification" type="checkbox" value="true" required />
                                 I am over 18 years of age and have read the Policy and Terms herein written and agree to be bound by them.<br>
                              </p>
                              <p>
                                 <input data-val="true"  name="Certification2" type="checkbox" value="true"/>
                                 I am the parent/legal guardian of the applicant/student who is a minor (below 18 years old). On behalf of the applicant/student, I have read the Policy and the Terms herein written, and agree to be bound by them. I understand that the Policy and Terms refer to the personal data of my child/ward.
                              </p>
                              <input type="submit" style="letter-spacing:4px; border-radius:0px; text-align:center; width:150px; border-width:2px; border-color:white; color:white;  font-size:14px;" value="SUBMIT" class="btn btn-success " />
                           </div>
                        </div>
                     </div>
                  </div>
               <?php echo form_close() ?>
            </div>
         </div>
      </div>
      <!--footer starts from here-->
      <footer class="footer">
      <div class="container bottom_border">
      <div class="row">
      <div class=" col-sm-4 col-md col-sm-4  col-12 col">
      <h5 class="headin5_amrc col_white_amrc pt2">Find us</h5>
      <!--headin5_amrc-->
      <p class="mb10">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
      <p><i class="fa fa-location-arrow"></i> 9878/25 sec 9 rohini 35 </p>
      <p><i class="fa fa-phone"></i>  +91-9999878398  </p>
      <p><i class="fa fa fa-envelope"></i> info@example.com  </p>


      </div>


      <div class=" col-sm-4 col-md  col-6 col">
      <h5 class="headin5_amrc col_white_amrc pt2">Quick links</h5>
      <!--headin5_amrc-->
      <ul class="footer_ul_amrc">
      <li><a href="">Home</a></li>
      <li><a href="">About</a></li>
      <li><a href="">Academics</a></li>
      <li><a href="">Admission</a></li>
      <li><a href="">Departmen</a></li>
      <li><a href="">Contact</a></li>
      </ul>
      <!--footer_ul_amrc ends here-->
      </div>


      <div class=" col-sm-4 col-md  col-6 col">
      <h5 class="headin5_amrc col_white_amrc pt2">Quick links</h5>
      <!--headin5_amrc-->
      <ul class="footer_ul_amrc">
      <li><a href="">Home</a></li>
      <li><a href="">About</a></li>
      <li><a href="">Academics</a></li>
      <li><a href="">Admission</a></li>
      <li><a href="">Departmen</a></li>
      <li><a href="">Contact</a></li>
      </ul>
      <!--footer_ul_amrc ends here-->
      </div>


      <div class=" col-sm-4 col-md  col-12 col">
      <h5 class="headin5_amrc col_white_amrc pt2">Follow us</h5>
      <!--headin5_amrc ends here-->

      <ul class="footer_ul2_amrc">
      <li><a href="#"><i class="fab fa-twitter fleft padding-right"></i> </a><p>Lorem Ipsum is simply dummy text of the printing...<a href="#">https://www.lipsum.com/</a></p></li>
      <li><a href="#"><i class="fab fa-twitter fleft padding-right"></i> </a><p>Lorem Ipsum is simply dummy text of the printing...<a href="#">https://www.lipsum.com/</a></p></li>
      <li><a href="#"><i class="fab fa-twitter fleft padding-right"></i> </a><p>Lorem Ipsum is simply dummy text of the printing...<a href="#">https://www.lipsum.com/</a></p></li>
      </ul>
      <!--footer_ul2_amrc ends here-->
      </div>
      </div>
      </div>


      <div class="container">
      <ul class="foote_bottom_ul_amrc">
      <li><a href="">Home</a></li>
      <li><a href="">About</a></li>
      <li><a href="">Academics</a></li>
      <li><a href="">Admission</a></li>
      <li><a href="">Departmen</a></li>
      <li><a href="">Contact</a></li>
      </ul>
      <!--foote_bottom_ul_amrc ends here-->
      <p class="text-center">Copyright @2019 | Copyright <a href="#">Colegio de San Juan Letran Calamba</a></p>
      <!--social_footer_ul ends here-->
      </div>

      </footer>

      <script src="<?php echo scripts_bundle()?>jquery-3.3.1.min.js"></script>
      <script src="<?php echo base_url()?>assets/popper/popper.min.js"></script>
      <script src="<?php echo base_url()?>assets/tether/tether.min.js"></script>
      <script src="<?php echo base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
      <script src="<?php echo scripts_bundle()?>jquery.chained.min.js"></script>
      <script src="<?php echo scripts_bundle()?>sweetalert2.min.js"></script>
      <script src="<?php echo base_url()?>assets/dropdown/js/script.min.js"></script>
      <script src="<?php echo base_url()?>assets/touchswipe/jquery.touch-swipe.min.js"></script>
      <script src="<?php echo base_url()?>assets/theme/js/script.js"></script>
      <script src="<?php echo scripts_bundle()?>global.js"></script>
      <script type="text/javascript">
      	$("#city").chained("#province"); 
      	var base_url = "<?php echo base_url()?>";
      	$("#city").on("change",function(){
      		$("#town").val("");
      		$("#town").prop("disabled",false);
      		$.ajax({
            type: "GET",
            url: base_url+"Admission/get_brgy",
            data:{'q':$("#city").val()},
            dataType: "json",
            beforeSend: function(){
            	loading();
            },
            success: function(data) {
            	var brgy = data.brgy;
            	close_loading();
            	var options = $("#town");
            	options.find('option')
					    .remove()
					    .end()
					    //don't forget error handling!
					    $.each(brgy, function(index,item) {
					    	options.append($("<option />").val(item.brgyCode).text(item.brgyDesc));
					    });
            },
            error: function(err) {
                console.log(err);
            }
        });
      		
      	})
      </script>
   </body>
</html>