<!DOCTYPE html>
<html class="dashboard_page">
<head>
  <!-- Site made with Mobirise Website Builder v4.9.2, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.9.2, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="<?php echo base_url()?>assets/images/letranlogo.png" type="image/x-icon">
  <meta name="description" content="">
  <title>Letran Admission</title>
  <link rel="stylesheet" href="<?php echo base_url()?>assets/web/assets/mobirise-icons/mobirise-icons.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/tether/tether.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="<?php echo styles_bundle()?>sweetalert2.min.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/main.css">
  <style type="text/css">
  .nav-link {
  	background: #fff !important;
  }
  	.nav-link.active {
  		    color: #fff !important;
    background-color: #8a8a8a !important;
  	}
  	body {
  		background: #dadada;
  	}
  	.tab-pane{
  		background: #fff;
  		padding: 20px 50px;
  	}
  	.btn_next {
  		margin-top: 20px;
  	}
  	.btn-default {
  		margin-top: 20px;
  		background: #eee;
  	}
  	p, label {
  		-webkit-user-select: none;
			-khtml-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
			-o-user-select: none;
			user-select: none;
  	}
  	.jst-hours {
		  float: left;
		  font-size:26px;
		}
		.jst-minutes {
		  float: left;
		  font-size:26px;
		}
		.jst-seconds {
		  float: left;
		  font-size:26px;
		}
		.jst-clearDiv {
		  clear: both;
		}
		.jst-timeout {
	    color: red;
	  }
  </style>
  	<script type="">
  		
  	</script>
</head>
<body>
	<div  class="container-fluid" style="background: #fff;padding:10px 20px;    box-shadow: 5px 2px 2px 0px rgba(0,0,0,0.75);">
		<div class="row">
			<div class="col-md-12">
				<center>
				<img src="<?php echo images_bundle()?>logo.png" class="img-fluid"  style="height: 70px;">
				</center>
			</div>
		</div>
	</div>
	<?php echo form_open("Exams/submit_exam","method='POST' id='exam_form'"); ?>
  <div class="container" style="min-height: 500px;margin-top: 50px;">
    <div class="row">
    	<div class="col-md-12">
    		<center>
    			<div class="timer" data-minutes-left=""></div>
    		</center>
    	</div>
    </div>
    <div class="row"> 
    	
        <div class="col-md-12">
        	<ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
        		<?php 
        		$x = 1;
        		foreach($subjects as $val) { ?>
					  	<?php if($x==1) { ?>
							  <li class="nav-item">
							    <a class="nav-link active" id="<?php echo $val?>-tab" data-toggle="tab" href="#<?php echo $val?>" role="tab" aria-controls="<?php echo $val?>" aria-selected="true"><?php echo $val?></a>
							  </li>
					  	<?php $x++; }else { ?>
					  		<li class="nav-item">
							    <a class="nav-link" id="<?php echo $val?>-tab" data-toggle="tab" href="#<?php echo $val?>" role="tab" aria-controls="<?php echo $val?>" aria-selected="false"><?php echo $val?></a>
							  </li>
					  	<?php } ?>
						<?php } ?>
					</ul>
					<div class="tab-content" id="myTabContent">
						<?php 
        		$x = 0;
        		$choice = ["A","B","C","D"];
        		$tab_next = 1;
        		
        		$num_questions = 0;
        		foreach($subjects as $val) { 
        			$tmp_courses = json_decode($courses[$x]);
        			$questions = json_decode($questions_desc[$x]);
        			$tmp_choices = json_decode($questions_choices[$x]);
        		?>
					  	<?php if($x==0) { ?>
							  <div class="tab-pane fade show active" id="<?php echo $val?>" role="tabpanel" aria-labelledby="<?php echo $val?>-tab">
							  	<?php
							  		//loop questions
						  			for($y=0; $y < count($questions);$y++) {
						  				$course = "";
						  				if($tmp_courses[$y] != ''){
						  					$course = implode(",",$tmp_courses[$y]);
						  				}
						  				
						  				$choices = $tmp_choices[$y];
						  				if($questions[$y] != ""){
							  				$num = $y + 1;
							  				$num_questions++;
							  				//print question
							  				echo '<div class="question">';
							  				echo "<p>".$num.'. '.$questions[$y]."</p>";
							  				for($j = 0; $j < count($choices); $j++) {
							  					//print choices
							  					if($j == 0) {
							  						echo '<div class="radio">
													  <label><input type="radio" name="answer['.$y.']" value="'.$choice[$j].'" checked>'.$choices[$j].'</label>
														</div>';
													} else {
														echo '<div class="radio">
														  <label><input type="radio" name="answer['.$y.']" value="'.$choice[$j].'" >'.$choices[$j].'</label>
														</div>';
													}
							  				}
							  				echo '<input type="hidden" name="subject['.$y.']" value="'.$val.'" />
							  						<input type="hidden" name="courses['.$y.']" value="'.$course.'" />
													  <input type="hidden" name="question['.$y.']" value="'.$questions[$y].'" />
													  
													  ';
												$str_choices = json_encode($choices, JSON_HEX_APOS);
												?>
												<input type="hidden" name="choices[<?php echo $y?>]" value='<?php echo $str_choices ?>' />
												<?php
							  				echo "<br></div>";
						  				}
						  			}
							  	?>
							  	<div class="row">
							  		<div class="col-md-12">
							  			<?php if(count($subjects ) == 1) { ?>
							  				<button type="submit" class="btn btn-success finish" style="padding:20px 50px;float: right;">Finish</button>
							  			<?php }else { ?>
							  			<button type="button" class="btn btn-success btn_next" style="padding:20px 50px;float: right;" data-next="<?php echo $subjects[$tab_next]; ?>">Next</button>
							  			<?php } ?>
							  		</div>
							  	</div>
							  	<?php $tab_next++ ?>
							  	
								</div>
					  	<?php }else { ?>
					  		<div class="tab-pane fade" id="<?php echo $val?>" role="tabpanel" aria-labelledby="<?php echo $val?>-tab">
					  			<?php
						  			for($y=0; $y < count($questions);$y++) {
						  				$choices = $tmp_choices[$y];
						  				$course = "";
						  				if($tmp_courses[$y] != ''){
						  					$course = implode(",",$tmp_courses[$y]);
						  				}
						  				if($questions[$y] != ""){
							  				$num = $y + 1;
							  				//print question
							  				echo "<p>".$num.'. '.$questions[$y]."</p>";
							  				for($j = 0; $j < count($choices); $j++) {
							  					//print choices
							  					if($j == 0) {
							  						echo '<div class="radio">
													  <label><input type="radio" name="answer['.$num_questions.']" value="'.$choice[$j].'" checked>'.$choices[$j].'</label>
														</div>';
													} else {
														echo '<div class="radio">
														  <label><input type="radio" name="answer['.$num_questions.']" value="'.$choice[$j].'" >'.$choices[$j].'</label>
														</div>';
													}
							  				}
							  				$str_choices = json_encode($choices);
							  				echo '<input type="hidden" name="subject['.$num_questions.']" value="'.$val.'" />
							  						<input type="hidden" name="courses['.$num_questions.']" value="'.$course.'" />
													  <input type="hidden" name="question['.$num_questions.']" value="'.$questions[$y].'" />
													  ';
												?>
												<input type="hidden" name="choices[<?php echo $num_questions?>]" value='<?php echo $str_choices ?>' />
												<?php
												 $num_questions++;
						  				}
						  			}
							  	?>
							  	<div class="row">
							  		<div class="col-md-12">
							  			<button type="button" class="btn btn-default btn_back" style="padding:20px 50px;float: left;" data-back="<?php echo $subjects[$tab_next-2]?>">Back</button>
							  			<?php 
							  				if(empty($subjects[$tab_next])){
							  				?>
							  				<button type="submit" class="btn btn-success finish" style="padding:20px 50px;float: right;">Finish</button>
							  				<?php
							  				}else{
							  				?>
							  				<button type="button" class="btn btn-success btn_next" style="padding:20px 50px;float: right;" data-next="<?php echo $subjects[$tab_next]?>">Next</button>
							  				<?php 
							  				} ?>
							  		</div>
							  	</div>
					  		</div>
					  	<?php $tab_next++;} ?>
						<?php $x++;  } ?>
						
					</div>
					<br>
					<br>
        </div>
       	
    </div> 
    <?php echo form_close();?> 
  </div>
  

  <?php $this->load->view('includes/footer') ?>


  <script src="<?php echo base_url()?>assets/web/assets/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url()?>assets/popper/popper.min.js"></script>
  <script src="<?php echo base_url()?>assets/tether/tether.min.js"></script>
  <script src="<?php echo base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?php echo scripts_bundle()?>sweetalert2.min.js"></script>
  <script src="<?php echo scripts_bundle()?>jquery.simple.timer.js"></script>
  <script src="<?php echo scripts_bundle()?>global.js"></script>
   <script type="text/javascript">

   	$(".finish").on("click",function(e){
   		e.preventDefault()
   		confirm_alert('Do you want to submit your exam?','You cannot edit your answers once submitted. ','info').then((response) => {
 		  		$("#exam_form").submit();
 			});
   	})

   	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		  var datastring = $("#exam_form").serialize();
		  var base_url = "<?php echo base_url()?>";
        $.ajax({
            type: "POST",
            url: base_url+"Exams/save_exam",
            data: datastring,
            dataType: "json",
            beforeSend: function() {
								loading();					    
					  },
            success: function(data) {
                close_loading();
            },
            error: function(err) {
                console.log(err);
            }
        });
		});

  	$(".btn_next").on("click",function(e){
  		e.preventDefault();
  		var next = $(this).data('next');
  		console.log(next);
  		$('#'+next+"-tab").click();
  	})

  	$(".btn_back").on("click",function(e){
  		e.preventDefault();
  		var back = $(this).data('back');
  		$('#'+back+"-tab").click();
  	})
  	if(getCookie("timeleft") == undefined || getCookie("timeleft") == null){
  			document.cookie = "timeleft=90";	
  			console.log('test')
  	}
  	
  	// JavaScript
		$(function(){

			var timeleft = parseInt(getCookie('timeleft'));
			console.log(timeleft);
			$('.timer').data('minutes-left',timeleft) 
		    $('.timer').startTimer({
		      onComplete: function(element){
		      	element.addClass('is-complete');
		        swal("Your time is up","submitting your exam","info");
		        setTimeout(function(){
		        	$("#exam_form").submit();
		        },2000)
		      },
			  loop: true,
			  loopInterval: 3,
		    });
		})

		var first = true;
		setInterval(function() {
			if(first){
				first = false;
			}else {
				var minutes = $(".jst-minutes").html()
				minutes = minutes.substring(0, minutes.length - 1);

				var tmp_minutes = $(".jst-minutes").html();
				var tmp_hour = $(".jst-hours").html()
				var tmp_secs = $(".jst-seconds").html()
				var time = tmp_hour+tmp_minutes;

				var timeArray = time.split(" ");
				timeArray = timeArray[0].split(":");

				var real_minutes = parseInt(timeArray[0])*60+parseInt(timeArray[1]);
				document.cookie =  "timeleft=" + real_minutes	

				if(minutes % 10 == 0 && tmp_secs == "00"){
					var datastring = $("#exam_form").serialize();
				  var base_url = "<?php echo base_url()?>";
	        $.ajax({
	            type: "POST",
	            url: base_url+"Exams/save_exam",
	            data: datastring,
	            dataType: "json",
	            beforeSend: function() {
									loading_text("Saving your exam please wait");					    
						  },
	            success: function(data) {
	                close_loading();
	            },
	            error: function(err) {
	                console.log(err);
	            }
	        });
				}
			}
		}, 1000);
		
  </script> 
</body>
</html>