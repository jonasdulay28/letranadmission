<!DOCTYPE html>
<html class="dashboard_page">
<head>
  <!-- Site made with Mobirise Website Builder v4.9.2, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.9.2, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="<?php echo base_url()?>assets/images/letranlogo.png" type="image/x-icon">
  <meta name="description" content="">
  <title>Letran Admission</title>
  <link rel="stylesheet" href="<?php echo base_url()?>assets/web/assets/mobirise-icons/mobirise-icons.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/tether/tether.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="<?php echo styles_bundle()?>sweetalert2.min.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/main.css">
  <style type="text/css">
  	.box:hover {
  		background: #004EA8;
  		-webkit-transition:  .5s; /* Safari */
  		transition:  .5s;
  	}
  	.box:hover span	{
  		color:#fff !important;
  	}
    .modal-dialog {
  position: absolute;
  top: 40%;
  left: 50%;
  transform: translate(-50%, -50%) !important;
}
  </style>
</head>
<body>
	<div  class="container-fluid" style="background: #fff;padding:10px 20px;    box-shadow: 5px 2px 2px 0px rgba(0,0,0,0.75);">
		<div class="row">
			<div class="col-md-12">
				<center>
				<img src="<?php echo images_bundle()?>logo.png" class="img-fluid"  style="height: 70px;">
				</center>
			</div>
		</div>
	</div>
  <div class="dashboard_banner" style="width: 100%;min-height: 150px;background:#004EA8 ">  

  </div>
  <div class="container " style="min-height: 500px;margin-top: 50px;">
    <div class="row"> 
        <div class="col-md-12">
        	<div class="card">
						  <div class="card-body">
						    <center><h1>Exam Results</h1>
						    <h4>Result: </h4>
						    <p><?php echo ($score < 33 ? "Probationary":"Regular") ?></p>
						    <h4>Course Suggestion:</h4>
						    <ul>
						    <?php foreach($course_suggestion as $val) { ?>
						    	<p><?php echo $val?></p>
						  	<?php } ?>
						  	</ul>
						  	<a href="<?php echo base_url()?>"><button class="btn btn-primary">Go back to dashboard</button></a>
						  </center>
						  </div>
						</div>
        </div>
    </div>  
  </div>
  <?php $this->load->view('includes/footer') ?>


  <script src="<?php echo base_url()?>assets/web/assets/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url()?>assets/popper/popper.min.js"></script>
  <script src="<?php echo base_url()?>assets/tether/tether.min.js"></script>
  <script src="<?php echo base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?php echo scripts_bundle()?>sweetalert2.min.js"></script>
  <script src="<?php echo scripts_bundle()?>global.js"></script>


</body>
</html>