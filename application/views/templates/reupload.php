<!DOCTYPE html>
<html>
<head>
  <!-- Site made with Mobirise Website Builder v4.9.2, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.9.2, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="<?php echo base_url()?>assets/images/letranlogo.png" type="image/x-icon">
  <meta name="description" content="">
  <title>Letran Admission</title>
  <link rel="stylesheet" href="<?php echo base_url()?>assets/web/assets/mobirise-icons/mobirise-icons.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/tether/tether.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap-reboot.min.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo styles_bundle()?>sweetalert2.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/main.css">
  <style type="text/css">
  	.app {
  		  display: -ms-flexbox;
		    display: flex;
		    -ms-flex-direction: column;
		    flex-direction: column;
		    min-height: 100vh;
        background-color: #e4e5e6;
  	}
    .swal2-title{
      font-family: 'Roboto' !important;
    }
  </style>
  
</head>
<body>
	<center>Reupload success <br><a href="<?php echo base_url('Dashboard')?>">Click here to go back to the dashboard</a></center>
</body>
</html>