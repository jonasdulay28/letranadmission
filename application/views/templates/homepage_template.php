<!DOCTYPE html>
<html >
<head>
  <!-- Site made with Mobirise Website Builder v4.9.2, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.9.2, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="<?php echo base_url()?>assets/images/letranlogo.png" type="image/x-icon">
  <meta name="description" content="">
  <title>Letran Admission</title>
  <link rel="stylesheet" href="<?php echo base_url()?>assets/web/assets/mobirise-icons/mobirise-icons.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/tether/tether.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/dropdown/css/style.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/theme/css/style.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/mobirise/css/mbr-additional.css" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/main.css">
</head>
<body>
<section class="header6 cid-ri36ny8Epx mbr-fullscreen" data-bg-video="https://player.vimeo.com/video/317244626?autoplay=0&loop=0" id="header6-0">
    <div class="mbr-overlay" style="opacity: 0.5; background-color: rgb(7, 59, 76);">
    </div>

    <div class="container home-container">
        <div class="row justify-content-md-center">
            <div class="mbr-white col-md-10">
                <img src="<?php echo images_bundle()?>letranlogo.png" class="img-fluid d-block mx-auto letranlogo">
                <h1 class="mbr-section-title align-center mbr-bold pb-3 mbr-fonts-style display-1 mb-0 home-header"><p>Colegio de San Juan de<br>Letran Calamba<br></p></h1>
                <!-- <p class="mbr-text align-center pb-3 mbr-fonts-style display-5"></p> -->
                <div class="mbr-section-btn align-center"><a class="btn btn-md btn-white-outline display-4 home_cta_btn" href="https://www.letran-calamba.edu.ph">VISIT</a>
                        <a class="btn btn-md btn-secondary display-4 home_cta_btn red" href="<?php echo base_url('Admission')?>">APPLY</a></div>
            </div>
        </div>
    </div>
</section>


  <script src="<?php echo base_url()?>assets/web/assets/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url()?>assets/popper/popper.min.js"></script>
  <script src="<?php echo base_url()?>assets/tether/tether.min.js"></script>
  <script src="<?php echo base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url()?>assets/dropdown/js/script.min.js"></script>
  <script src="<?php echo base_url()?>assets/touchswipe/jquery.touch-swipe.min.js"></script>
  <script src="<?php echo base_url()?>assets/ytplayer/jquery.mb.ytplayer.min.js"></script>
  <script src="<?php echo base_url()?>assets/vimeoplayer/jquery.mb.vimeo_player.js"></script>
  <script src="<?php echo base_url()?>assets/smoothscroll/smooth-scroll.js"></script>
  <script src="<?php echo base_url()?>assets/theme/js/script.js"></script>
  
  
</body>
</html>