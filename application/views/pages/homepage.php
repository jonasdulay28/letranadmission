<section class="header6 cid-ri36ny8Epx mbr-fullscreen homebg" id="header6-0" >
    <div class="mbr-overlay" style="opacity: 0.5; background-color: rgb(7, 59, 76);">
    </div>
    <div class="container home-container">
        <div class="row justify-content-md-center">
            <div class="mbr-white col-md-10">
                <img src="<?php echo images_bundle()?>letranlogo.png" class="img-fluid d-block mx-auto letranlogo">
                <h1 class="mbr-section-title align-center mbr-bold pb-3 mbr-fonts-style display-1 mb-0 home-header"><p>Colegio de San Juan de Letran Calamba<br></p></h1>
                <!-- <p class="mbr-text align-center pb-3 mbr-fonts-style display-5"></p> -->
                <div class="mbr-section-btn align-center"><a class="btn btn-md btn-white display-4 home_cta_btn" href="https://www.letran-calamba.edu.ph/">VISIT</a>
                        <a class="btn btn-md btn-secondary display-4 home_cta_btn" href="">APPLY</a></div>
            </div>
        </div>
    </div>
</section>
