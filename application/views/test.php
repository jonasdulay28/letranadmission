<div style="background: #D0D7DF;width: 100%;padding: 30px;font-family: 'Arial'">
	<h3>EXAMINATION PERMIT</h3>
	<h3 style="font-weight: normal;">Academic Year <?php echo date("Y").' - ';?> <?php echo date("Y") + 1?></h3>
	<p>Status: Transferee</p>
	<p>Examinee: Transferee</p>
	<p>Exam Date: Transferee</p>
	<p>Time: Transferee</p>
	<p>Room: Transferee</p>
	<br>
	<p>Issued by: Transferee</p>
	<p>Date Issued: Transferee</p>
	<p>Reference number: Transferee</p>
	<p><b>INSTRUCTIONS:</b></p>
	<ul>
		<li>Examinees WITHOUT examination permit cannot take the test</li>
		<li>Please be at the examination site at least 15 minutes before the test</li>
		<li>Late examiness will not be allowed to take the test</li>
		<li>Bring 2 sharpened pencils with eraser</li>
		<li>High School Examiness should bring 1 (12 inches) ruler</li>
	</ul>
</div>