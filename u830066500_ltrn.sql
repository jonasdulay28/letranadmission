-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 18, 2019 at 12:04 AM
-- Server version: 10.2.22-MariaDB
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u830066500_ltrn`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `id` int(100) NOT NULL,
  `question_desc` text NOT NULL,
  `question_choices` text NOT NULL,
  `question_answer` text NOT NULL,
  `subject_name` text NOT NULL,
  `courses` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` int(2) NOT NULL DEFAULT 0,
  `reference_number` varchar(15) NOT NULL,
  `student_answer` text NOT NULL,
  `num_per_subject` text NOT NULL,
  `score` int(10) DEFAULT NULL,
  `course_suggestion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `application`
--

CREATE TABLE `application` (
  `application_id` int(11) NOT NULL,
  `first_name` varchar(200) NOT NULL,
  `last_name` varchar(200) NOT NULL,
  `middle_name` varchar(200) NOT NULL,
  `suffix` varchar(100) NOT NULL,
  `address` varchar(500) NOT NULL,
  `subdivision` varchar(500) NOT NULL,
  `postal_code` varchar(10) NOT NULL,
  `city` varchar(200) NOT NULL,
  `town` varchar(200) NOT NULL,
  `birthday` varchar(200) NOT NULL,
  `birth_place` varchar(200) NOT NULL,
  `landline` varchar(200) NOT NULL,
  `mobile` varchar(200) NOT NULL,
  `citizenship` varchar(200) NOT NULL,
  `civil_status` varchar(200) NOT NULL,
  `religion` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `gender` varchar(200) NOT NULL,
  `father_name` varchar(200) NOT NULL,
  `father_contact` varchar(200) NOT NULL,
  `father_email` varchar(200) NOT NULL,
  `mother_name` varchar(200) NOT NULL,
  `mother_contact` varchar(200) DEFAULT NULL,
  `mother_email` varchar(200) DEFAULT NULL,
  `no_sibling` varchar(10) DEFAULT NULL,
  `guardian_name` varchar(200) DEFAULT NULL,
  `guardian_address` varchar(500) DEFAULT NULL,
  `guardian_relationship` varchar(100) DEFAULT NULL,
  `guardian_contact` varchar(100) DEFAULT NULL,
  `former_school` varchar(500) DEFAULT NULL,
  `program` varchar(100) DEFAULT NULL,
  `school_year` varchar(20) DEFAULT NULL,
  `school_address` varchar(500) DEFAULT NULL,
  `gwa_first` varchar(10) DEFAULT NULL,
  `gwa_second` varchar(10) DEFAULT NULL,
  `track_type` varchar(100) DEFAULT NULL,
  `track_sub_type` varchar(100) DEFAULT NULL,
  `honors` varchar(500) NOT NULL,
  `apply_reason` varchar(100) NOT NULL,
  `apply_others` varchar(200) NOT NULL,
  `referal` varchar(100) NOT NULL,
  `referal_others` varchar(200) NOT NULL,
  `id_picture` varchar(100) NOT NULL,
  `certificate_of_candidacy` varchar(100) NOT NULL,
  `nso_psa` varchar(100) NOT NULL,
  `report_card` varchar(100) NOT NULL,
  `copy_of_grade` varchar(100) NOT NULL,
  `status` varchar(2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `reference_number` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `application`
--

INSERT INTO `application` (`application_id`, `first_name`, `last_name`, `middle_name`, `suffix`, `address`, `subdivision`, `postal_code`, `city`, `town`, `birthday`, `birth_place`, `landline`, `mobile`, `citizenship`, `civil_status`, `religion`, `email`, `gender`, `father_name`, `father_contact`, `father_email`, `mother_name`, `mother_contact`, `mother_email`, `no_sibling`, `guardian_name`, `guardian_address`, `guardian_relationship`, `guardian_contact`, `former_school`, `program`, `school_year`, `school_address`, `gwa_first`, `gwa_second`, `track_type`, `track_sub_type`, `honors`, `apply_reason`, `apply_others`, `referal`, `referal_others`, `id_picture`, `certificate_of_candidacy`, `nso_psa`, `report_card`, `copy_of_grade`, `status`, `created_at`, `updated_at`, `reference_number`) VALUES
(5, 'KIEL ALEXIS', 'RAZO', '', '', '012', '', '', 'LAGUNA', 'SINILOAN', '2019-03-06', 'LAGUNA', '', '', 'FILIPINO', 'SINGLE', '', 'KIELRAZO@GMAIL.COM', 'M', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'ACADEMIC', 'STEM', '', '', '', '', '', '', '', '', '', '', '', '2019-03-06 10:45:46', '2019-03-06 10:45:46', '');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `user_agent`, `last_activity`, `data`, `timestamp`) VALUES
('01k8auv85d2lih5s6u70tkh11s1f9fm0', '::1', '', 0, '__ci_last_regenerate|i:1550858574;ses_id|s:32:\"SFBEc1RSYTc3WStiYXc0UWFzam1jZz09\";', '0000-00-00 00:00:00'),
('08hksnt7p8m79a1jphttgd34nsctniof', '::1', '', 0, '__ci_last_regenerate|i:1551013634;', '0000-00-00 00:00:00'),
('0qfdghdq9m56clogh33cokqts8kkp0qc', '::1', '', 0, '__ci_last_regenerate|i:1550818185;', '0000-00-00 00:00:00'),
('14v0url3fep1al4otchfrvti7u9d3v22', '::1', '', 0, '__ci_last_regenerate|i:1550985508;ses_id|s:32:\"K2xIQmtFN3Z5VzlmQ21sNEVBcTluUT09\";reference_number|s:9:\"201974084\";__ci_vars|a:2:{s:16:\"reference_number\";s:3:\"old\";s:8:\"password\";s:3:\"old\";}password|s:12:\"dasdasdasdas\";', '0000-00-00 00:00:00'),
('2fda6ujkarra3goo3ac1mn80u2deoj1d', '::1', '', 0, '__ci_last_regenerate|i:1550929020;ses_id|s:32:\"ZzF4YUtWQXo3dmZvNTMySmU0UndGQT09\";ses_role|s:32:\"YytMNTQvSSt6YzVzRkZRbW9JT0Jadz09\";', '0000-00-00 00:00:00'),
('2mdserhidn9abu2ffa6salv0e36br3gi', '::1', '', 0, '__ci_last_regenerate|i:1550855897;ses_id|s:32:\"SFBEc1RSYTc3WStiYXc0UWFzam1jZz09\";', '0000-00-00 00:00:00'),
('2r5jv8eui4o72gb78e5k1i3tlf7afrhc', '::1', '', 0, '__ci_last_regenerate|i:1550995849;', '0000-00-00 00:00:00'),
('351j7fpj3cucaq0s5cd9l4ael94a5hiq', '::1', '', 0, '__ci_last_regenerate|i:1550820521;', '0000-00-00 00:00:00'),
('3d6tegep0dkv582qdng9sommocor780t', '::1', '', 0, '__ci_last_regenerate|i:1550858205;ses_id|s:32:\"SFBEc1RSYTc3WStiYXc0UWFzam1jZz09\";', '0000-00-00 00:00:00'),
('3jlpm0o653f8mhm1jtsjvav85gaucg3e', '::1', '', 0, '__ci_last_regenerate|i:1550985822;ses_id|s:32:\"K2xIQmtFN3Z5VzlmQ21sNEVBcTluUT09\";', '0000-00-00 00:00:00'),
('3mnrep1mb4lvpm3k3ohu6mikd7tpo5a4', '::1', '', 0, '__ci_last_regenerate|i:1551005651;', '0000-00-00 00:00:00'),
('42u01kr0spd3hpkot051an2326bkpcoq', '::1', '', 0, '__ci_last_regenerate|i:1550848489;', '0000-00-00 00:00:00'),
('4p1o1h5q4otudb4unl8m6h6bk91iummp', '::1', '', 0, '__ci_last_regenerate|i:1550825171;', '0000-00-00 00:00:00'),
('5dduhe7kahmcpj6t6hqi2hd287s151h1', '::1', '', 0, '__ci_last_regenerate|i:1551002042;', '0000-00-00 00:00:00'),
('5dstafpo0rrhmppnhpjje4bhf55enods', '::1', '', 0, '__ci_last_regenerate|i:1550822589;', '0000-00-00 00:00:00'),
('5ehr3ohoav0233rg1epcsnv2r0kmeg64', '::1', '', 0, '__ci_last_regenerate|i:1550819473;', '0000-00-00 00:00:00'),
('5ql35gv0cqhpl84pp1m2ol6mgv0a279n', '::1', '', 0, '__ci_last_regenerate|i:1551007691;', '0000-00-00 00:00:00'),
('66b1837ikpgqf27ik1i48favq254g6j3', '::1', '', 0, '__ci_last_regenerate|i:1551013307;', '0000-00-00 00:00:00'),
('66k3s0b9t86p0jojfok6diu4ijmqq15e', '::1', '', 0, '__ci_last_regenerate|i:1550920814;ses_id|s:32:\"ZzF4YUtWQXo3dmZvNTMySmU0UndGQT09\";ses_role|s:32:\"YytMNTQvSSt6YzVzRkZRbW9JT0Jadz09\";', '0000-00-00 00:00:00'),
('67hjla71h6086j21os6fck0pb6qq0715', '::1', '', 0, '__ci_last_regenerate|i:1550921115;ses_id|s:32:\"ZzF4YUtWQXo3dmZvNTMySmU0UndGQT09\";ses_role|s:32:\"YytMNTQvSSt6YzVzRkZRbW9JT0Jadz09\";', '0000-00-00 00:00:00'),
('6af5i5rkivbhtvm65ahfdt4p4gihsc6n', '::1', '', 0, '__ci_last_regenerate|i:1550916342;ses_id|s:32:\"ZzF4YUtWQXo3dmZvNTMySmU0UndGQT09\";ses_role|s:32:\"YytMNTQvSSt6YzVzRkZRbW9JT0Jadz09\";', '0000-00-00 00:00:00'),
('708383f0jc6iihdnlpampi9370o38git', '::1', '', 0, '__ci_last_regenerate|i:1550976830;ses_id|s:32:\"K2xIQmtFN3Z5VzlmQ21sNEVBcTluUT09\";', '0000-00-00 00:00:00'),
('7eridd18ifd7dfg491s5pu0r02e3p5j3', '::1', '', 0, '__ci_last_regenerate|i:1550819167;', '0000-00-00 00:00:00'),
('7hm786usnus2v7kmo0ppjvb3ucb9o8h8', '::1', '', 0, '__ci_last_regenerate|i:1550908095;ses_id|s:32:\"ZzF4YUtWQXo3dmZvNTMySmU0UndGQT09\";ses_role|s:32:\"YytMNTQvSSt6YzVzRkZRbW9JT0Jadz09\";', '0000-00-00 00:00:00'),
('7mpftrs3l3sn75oqted7jo2op482785v', '::1', '', 0, '__ci_last_regenerate|i:1550907713;ses_id|s:32:\"ZzF4YUtWQXo3dmZvNTMySmU0UndGQT09\";ses_role|s:32:\"YytMNTQvSSt6YzVzRkZRbW9JT0Jadz09\";', '0000-00-00 00:00:00'),
('7ogeja38udha34go3j3bgrc2jamf5mva', '::1', '', 0, '__ci_last_regenerate|i:1551008982;', '0000-00-00 00:00:00'),
('88lvmlf713vi18eehuqp5egfer10snd6', '::1', '', 0, '__ci_last_regenerate|i:1550905765;ses_id|s:32:\"ZzF4YUtWQXo3dmZvNTMySmU0UndGQT09\";ses_role|s:32:\"YytMNTQvSSt6YzVzRkZRbW9JT0Jadz09\";', '0000-00-00 00:00:00'),
('8j5rtpmmrunbj6fg4p50armn9ojp2hao', '::1', '', 0, '__ci_last_regenerate|i:1550998656;', '0000-00-00 00:00:00'),
('94daujjn4t2kc12aprrtt2t28mpeblfe', '::1', '', 0, '__ci_last_regenerate|i:1550806214;', '0000-00-00 00:00:00'),
('9fucp8hcsg814ltuphe3dplictkg8aqp', '::1', '', 0, '__ci_last_regenerate|i:1550856221;ses_id|s:32:\"SFBEc1RSYTc3WStiYXc0UWFzam1jZz09\";', '0000-00-00 00:00:00'),
('9lq9v24gh1lo12t2a2qg3dtu66m027s4', '::1', '', 0, '__ci_last_regenerate|i:1550848796;ses_id|s:32:\"SFBEc1RSYTc3WStiYXc0UWFzam1jZz09\";', '0000-00-00 00:00:00'),
('9mtc7r80ggb74c9f7caum77dhdici33h', '::1', '', 0, '__ci_last_regenerate|i:1550807063;', '0000-00-00 00:00:00'),
('9trbs0d2skm8jlcbi7ntsf479bc9pmel', '::1', '', 0, '__ci_last_regenerate|i:1550904630;', '0000-00-00 00:00:00'),
('9u4c65m46fgi6a81aliir1mra488f3i3', '::1', '', 0, '__ci_last_regenerate|i:1550999104;', '0000-00-00 00:00:00'),
('adjn29sbr1p00cg1v80043inhbt4t7cq', '::1', '', 0, '__ci_last_regenerate|i:1551006048;', '0000-00-00 00:00:00'),
('ar8co5mhn4q097oikv4rsijkahfpm1pb', '::1', '', 0, '__ci_last_regenerate|i:1550923312;ses_id|s:32:\"ZzF4YUtWQXo3dmZvNTMySmU0UndGQT09\";ses_role|s:32:\"YytMNTQvSSt6YzVzRkZRbW9JT0Jadz09\";', '0000-00-00 00:00:00'),
('b08tmmlqrjpind405qtggk09amm8hqsl', '::1', '', 0, '__ci_last_regenerate|i:1551001424;', '0000-00-00 00:00:00'),
('b93f90ofo62eo39s81oh3cacpm7nf3c6', '::1', '', 0, '__ci_last_regenerate|i:1550924152;ses_id|s:32:\"ZzF4YUtWQXo3dmZvNTMySmU0UndGQT09\";ses_role|s:32:\"YytMNTQvSSt6YzVzRkZRbW9JT0Jadz09\";', '0000-00-00 00:00:00'),
('cnkuie8344letc61u2ivbq95m1jfl9hp', '::1', '', 0, '__ci_last_regenerate|i:1550981471;ses_id|s:32:\"K2xIQmtFN3Z5VzlmQ21sNEVBcTluUT09\";', '0000-00-00 00:00:00'),
('d2qrg77l59kohvgi8p9i7la1kqirl42f', '::1', '', 0, '__ci_last_regenerate|i:1551000194;', '0000-00-00 00:00:00'),
('deebmrqmfj0oj7b94j283ndqlm1b0a0e', '::1', '', 0, '__ci_last_regenerate|i:1550904320;', '0000-00-00 00:00:00'),
('dfhej4vcma33sg1slf597tl57am8qkp3', '::1', '', 0, '__ci_last_regenerate|i:1551000496;', '0000-00-00 00:00:00'),
('dq9nob9jpba8l3d6280m06ktb86afg2f', '::1', '', 0, '__ci_last_regenerate|i:1550844912;', '0000-00-00 00:00:00'),
('dtf45ir68a567uli1gepkd3n1l8kqrd1', '::1', '', 0, '__ci_last_regenerate|i:1550855548;ses_id|s:32:\"SFBEc1RSYTc3WStiYXc0UWFzam1jZz09\";', '0000-00-00 00:00:00'),
('f8k98bukck2berdov40ub0un6qrd1dm0', '::1', '', 0, '__ci_last_regenerate|i:1550817153;', '0000-00-00 00:00:00'),
('fdsk2i6hlb9j6sjjhe0mq5r3v6r9ndf3', '::1', '', 0, '__ci_last_regenerate|i:1550929758;ses_id|s:32:\"ZzF4YUtWQXo3dmZvNTMySmU0UndGQT09\";ses_role|s:32:\"YytMNTQvSSt6YzVzRkZRbW9JT0Jadz09\";', '0000-00-00 00:00:00'),
('fhc5nvv2d9l6bs1sp822sohei18o77of', '::1', '', 0, '__ci_last_regenerate|i:1550820845;', '0000-00-00 00:00:00'),
('fpb9r5b6kfla7m75r8rhncaajuki24cm', '::1', '', 0, '__ci_last_regenerate|i:1550916041;ses_id|s:32:\"ZzF4YUtWQXo3dmZvNTMySmU0UndGQT09\";ses_role|s:32:\"YytMNTQvSSt6YzVzRkZRbW9JT0Jadz09\";', '0000-00-00 00:00:00'),
('fr0n4dnbv08h6e0ovqg8sbha7ttpa9i2', '::1', '', 0, '__ci_last_regenerate|i:1550841027;', '0000-00-00 00:00:00'),
('fslgm5i8onmb5tlcnrn5ckulnt97ovkg', '::1', '', 0, '__ci_last_regenerate|i:1550857557;ses_id|s:32:\"SFBEc1RSYTc3WStiYXc0UWFzam1jZz09\";', '0000-00-00 00:00:00'),
('fvvb6924br0rm3vnhn0lcusgvbi953fl', '::1', '', 0, '__ci_last_regenerate|i:1550825476;', '0000-00-00 00:00:00'),
('grfmtmvadpkmnlma2a8ndlvt3dot9q7o', '::1', '', 0, '__ci_last_regenerate|i:1550922647;ses_id|s:32:\"ZzF4YUtWQXo3dmZvNTMySmU0UndGQT09\";ses_role|s:32:\"YytMNTQvSSt6YzVzRkZRbW9JT0Jadz09\";', '0000-00-00 00:00:00'),
('h0tv2gdv6mmqhqvd8sa8jhggkt8serhs', '::1', '', 0, '__ci_last_regenerate|i:1550920322;ses_id|s:32:\"ZzF4YUtWQXo3dmZvNTMySmU0UndGQT09\";ses_role|s:32:\"YytMNTQvSSt6YzVzRkZRbW9JT0Jadz09\";', '0000-00-00 00:00:00'),
('h4s3ira9vabhp007cmcas8ooncaoibtk', '::1', '', 0, '__ci_last_regenerate|i:1550915721;ses_id|s:32:\"ZzF4YUtWQXo3dmZvNTMySmU0UndGQT09\";ses_role|s:32:\"YytMNTQvSSt6YzVzRkZRbW9JT0Jadz09\";', '0000-00-00 00:00:00'),
('h60k98digkbrej1phh09481asctkuauq', '::1', '', 0, '__ci_last_regenerate|i:1550976242;ses_id|s:32:\"K2xIQmtFN3Z5VzlmQ21sNEVBcTluUT09\";', '0000-00-00 00:00:00'),
('hh6kn6e65m7t6dmpttbphp0otg8u8rp9', '::1', '', 0, '__ci_last_regenerate|i:1551013634;', '0000-00-00 00:00:00'),
('i1m289cvebe8k1us82i4ah2kjqv5t7ni', '::1', '', 0, '__ci_last_regenerate|i:1550818736;', '0000-00-00 00:00:00'),
('iameklu78i7eht91f30ka7i4agjjdhrt', '::1', '', 0, '__ci_last_regenerate|i:1550855147;ses_id|s:32:\"SFBEc1RSYTc3WStiYXc0UWFzam1jZz09\";', '0000-00-00 00:00:00'),
('jcs95linr30lpg60ou9768q8gcdv2mle', '::1', '', 0, '__ci_last_regenerate|i:1550807063;', '0000-00-00 00:00:00'),
('jl8p9kk8lr63qckkvvhif84o40cuesnn', '::1', '', 0, '__ci_last_regenerate|i:1550854469;ses_id|s:32:\"SFBEc1RSYTc3WStiYXc0UWFzam1jZz09\";', '0000-00-00 00:00:00'),
('k83kp9cpnji6538vcd4ndcvuacqrkf2i', '::1', '', 0, '__ci_last_regenerate|i:1550996163;', '0000-00-00 00:00:00'),
('k9k9pq9bdet87he920d9kct898kdv88h', '::1', '', 0, '__ci_last_regenerate|i:1550857089;ses_id|s:32:\"SFBEc1RSYTc3WStiYXc0UWFzam1jZz09\";', '0000-00-00 00:00:00'),
('kojji0d6ohsgii7kg8bq1hasf5l5ndsl', '::1', '', 0, '__ci_last_regenerate|i:1550922299;ses_id|s:32:\"ZzF4YUtWQXo3dmZvNTMySmU0UndGQT09\";ses_role|s:32:\"YytMNTQvSSt6YzVzRkZRbW9JT0Jadz09\";', '0000-00-00 00:00:00'),
('ku6rasdqf3206fvbvhierno6dkpir7ie', '::1', '', 0, '__ci_last_regenerate|i:1550985194;ses_id|s:32:\"K2xIQmtFN3Z5VzlmQ21sNEVBcTluUT09\";', '0000-00-00 00:00:00'),
('l3quscksk5mio8q4peah2l45ubddb2r6', '::1', '', 0, '__ci_last_regenerate|i:1550981117;ses_id|s:32:\"K2xIQmtFN3Z5VzlmQ21sNEVBcTluUT09\";', '0000-00-00 00:00:00'),
('l7mcql9rr08blb2sp5mghq667nv20sob', '::1', '', 0, '__ci_last_regenerate|i:1550860296;ses_id|s:32:\"SFBEc1RSYTc3WStiYXc0UWFzam1jZz09\";', '0000-00-00 00:00:00'),
('lp2pfuen750tv5755s2okrv2avb04q0f', '::1', '', 0, '__ci_last_regenerate|i:1550805862;', '0000-00-00 00:00:00'),
('m03sp3en9qlb1pdajcbfei0ngo060vii', '::1', '', 0, '__ci_last_regenerate|i:1550822283;', '0000-00-00 00:00:00'),
('m0c1g6b2bo51qgbkuihev2fsu4ntit4k', '::1', '', 0, '__ci_last_regenerate|i:1550905453;ses_id|s:32:\"ZzF4YUtWQXo3dmZvNTMySmU0UndGQT09\";ses_role|s:32:\"YytMNTQvSSt6YzVzRkZRbW9JT0Jadz09\";', '0000-00-00 00:00:00'),
('mkmn0lvd6mriuu4056e771hr3v5suoro', '::1', '', 0, '__ci_last_regenerate|i:1551012997;', '0000-00-00 00:00:00'),
('muq42suqg1va0vh07bdoqvbauot3dfuh', '::1', '', 0, '__ci_last_regenerate|i:1550929430;ses_id|s:32:\"ZzF4YUtWQXo3dmZvNTMySmU0UndGQT09\";ses_role|s:32:\"YytMNTQvSSt6YzVzRkZRbW9JT0Jadz09\";', '0000-00-00 00:00:00'),
('n4rin7n869cf7j5v0koj7mf5hbdbe0ah', '::1', '', 0, '__ci_last_regenerate|i:1550839011;', '0000-00-00 00:00:00'),
('neungllhtosktdr7lm3h8l6nois54he6', '::1', '', 0, '__ci_last_regenerate|i:1550856711;ses_id|s:32:\"SFBEc1RSYTc3WStiYXc0UWFzam1jZz09\";', '0000-00-00 00:00:00'),
('o80rfjmqn0df0o6jnspkcpjjl5iegbel', '::1', '', 0, '__ci_last_regenerate|i:1550847963;', '0000-00-00 00:00:00'),
('ocnuqickmbhnf5qt8ojr8rcqsdjpdbqi', '::1', '', 0, '__ci_last_regenerate|i:1550930086;ses_id|s:32:\"ZzF4YUtWQXo3dmZvNTMySmU0UndGQT09\";ses_role|s:32:\"YytMNTQvSSt6YzVzRkZRbW9JT0Jadz09\";', '0000-00-00 00:00:00'),
('p00kpkcsm5v5ca5l6ave1gc7hd1lcpnk', '::1', '', 0, '__ci_last_regenerate|i:1550978362;ses_id|s:32:\"K2xIQmtFN3Z5VzlmQ21sNEVBcTluUT09\";', '0000-00-00 00:00:00'),
('piua71lkg2ic71dg551qu10hloh4c7k7', '::1', '', 0, '__ci_last_regenerate|i:1550819838;', '0000-00-00 00:00:00'),
('q1c94dtamnqcuvj752lv0ucsguot66gf', '::1', '', 0, '__ci_last_regenerate|i:1550854832;ses_id|s:32:\"SFBEc1RSYTc3WStiYXc0UWFzam1jZz09\";', '0000-00-00 00:00:00'),
('qgt3mmg9hbov2lrjnukvgdq0kpcn687h', '::1', '', 0, '__ci_last_regenerate|i:1550825476;', '0000-00-00 00:00:00'),
('qotor2t22996jnfvli0hnltmaarg059m', '::1', '', 0, '__ci_last_regenerate|i:1550913528;ses_id|s:32:\"ZzF4YUtWQXo3dmZvNTMySmU0UndGQT09\";ses_role|s:32:\"YytMNTQvSSt6YzVzRkZRbW9JT0Jadz09\";', '0000-00-00 00:00:00'),
('quvvrq6oaj85ufip53ltb3doom722tq2', '::1', '', 0, '__ci_last_regenerate|i:1550844558;', '0000-00-00 00:00:00'),
('qvamsjj01hklnddo30h756jlsdd5l79n', '::1', '', 0, '__ci_last_regenerate|i:1550975933;ses_id|s:32:\"K2xIQmtFN3Z5VzlmQ21sNEVBcTluUT09\";', '0000-00-00 00:00:00'),
('r5jo3t8tf4859fjvdd39km51mpcms24b', '::1', '', 0, '__ci_last_regenerate|i:1550847075;', '0000-00-00 00:00:00'),
('rhpc00h8fv6ahjpaghqb5l00a7dvckq6', '::1', '', 0, '__ci_last_regenerate|i:1550928719;ses_id|s:32:\"ZzF4YUtWQXo3dmZvNTMySmU0UndGQT09\";ses_role|s:32:\"YytMNTQvSSt6YzVzRkZRbW9JT0Jadz09\";', '0000-00-00 00:00:00'),
('rl08flofmuip5d4sh94keo5m7g4ljbsr', '::1', '', 0, '__ci_last_regenerate|i:1550817462;', '0000-00-00 00:00:00'),
('rte8gh4dhqv885vmleuhcq9nhcppelnq', '::1', '', 0, '__ci_last_regenerate|i:1551008253;', '0000-00-00 00:00:00'),
('ruppakh161pj2sghucbc51q624p7pd0t', '::1', '', 0, '__ci_last_regenerate|i:1551010311;', '0000-00-00 00:00:00'),
('rvfv2smialf4t9o9msl3lvnhtnplga6v', '::1', '', 0, '__ci_last_regenerate|i:1550840664;', '0000-00-00 00:00:00'),
('s9t4h6d2qfp4dkgmng7sl9qajr2dnl27', '::1', '', 0, '__ci_last_regenerate|i:1550860296;ses_id|s:32:\"SFBEc1RSYTc3WStiYXc0UWFzam1jZz09\";', '0000-00-00 00:00:00'),
('siv3al5k1lf7ni6jntatqhqlj2s0p65f', '::1', '', 0, '__ci_last_regenerate|i:1551007079;', '0000-00-00 00:00:00'),
('sq69o6i0p2nf1immj2gsuu2a8anku1rl', '::1', '', 0, '__ci_last_regenerate|i:1550913835;ses_id|s:32:\"ZzF4YUtWQXo3dmZvNTMySmU0UndGQT09\";ses_role|s:32:\"YytMNTQvSSt6YzVzRkZRbW9JT0Jadz09\";', '0000-00-00 00:00:00'),
('tku0arhl0micktkv6l4m35gv0l17dotr', '::1', '', 0, '__ci_last_regenerate|i:1550820159;', '0000-00-00 00:00:00'),
('tq7ei8esliu4e1tq18jg7jjstap3grm8', '::1', '', 0, '__ci_last_regenerate|i:1550817840;', '0000-00-00 00:00:00'),
('udr9b5vvq0t447a72mtem1aelbus4slg', '::1', '', 0, '__ci_last_regenerate|i:1550985822;ses_id|s:32:\"K2xIQmtFN3Z5VzlmQ21sNEVBcTluUT09\";', '0000-00-00 00:00:00'),
('ufvn2rnh0m3mbjv88gelgucep07eh7gp', '::1', '', 0, '__ci_last_regenerate|i:1550904938;', '0000-00-00 00:00:00'),
('uh8vejk7ben7les1i4mfsva6chcc2fld', '::1', '', 0, '__ci_last_regenerate|i:1550930086;ses_id|s:32:\"ZzF4YUtWQXo3dmZvNTMySmU0UndGQT09\";ses_role|s:32:\"YytMNTQvSSt6YzVzRkZRbW9JT0Jadz09\";', '0000-00-00 00:00:00'),
('uh9mmujrcp5ge23tk2ij1qqai4rg89vk', '::1', '', 0, '__ci_last_regenerate|i:1550847410;', '0000-00-00 00:00:00'),
('upg57j0faevshuvreafv36f967ho86b6', '::1', '', 0, '__ci_last_regenerate|i:1550921523;ses_id|s:32:\"ZzF4YUtWQXo3dmZvNTMySmU0UndGQT09\";ses_role|s:32:\"YytMNTQvSSt6YzVzRkZRbW9JT0Jadz09\";', '0000-00-00 00:00:00'),
('v640ra9999mq814v2q78bkgmbl2turqv', '::1', '', 0, '__ci_last_regenerate|i:1551008659;', '0000-00-00 00:00:00'),
('vaeefnqm6insbeurjnq0svld60j7b03j', '::1', '', 0, '__ci_last_regenerate|i:1551006372;', '0000-00-00 00:00:00'),
('vcco23jq4d58a6vpq3eqlea3qhqtpdp3', '::1', '', 0, '__ci_last_regenerate|i:1550913204;ses_id|s:32:\"ZzF4YUtWQXo3dmZvNTMySmU0UndGQT09\";ses_role|s:32:\"YytMNTQvSSt6YzVzRkZRbW9JT0Jadz09\";', '0000-00-00 00:00:00'),
('vkda2t60cbjjj73hn77v7fkn87sb08qd', '::1', '', 0, '__ci_last_regenerate|i:1550923002;ses_id|s:32:\"ZzF4YUtWQXo3dmZvNTMySmU0UndGQT09\";ses_role|s:32:\"YytMNTQvSSt6YzVzRkZRbW9JT0Jadz09\";', '0000-00-00 00:00:00'),
('vkm5p1sr70367u60lh1cihas0ioicuhh', '::1', '', 0, '__ci_last_regenerate|i:1550974880;', '0000-00-00 00:00:00'),
('vphihc0rdl3vckh1o8dfa9r1ltlapqod', '::1', '', 0, '__ci_last_regenerate|i:1550924466;ses_id|s:32:\"ZzF4YUtWQXo3dmZvNTMySmU0UndGQT09\";ses_role|s:32:\"YytMNTQvSSt6YzVzRkZRbW9JT0Jadz09\";', '0000-00-00 00:00:00'),
('vuis8q5ffon0h1r7ao3stro5ih9a21lh', '::1', '', 0, '__ci_last_regenerate|i:1550843601;', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `course_id` int(10) NOT NULL,
  `course_name` varchar(500) NOT NULL,
  `min_grade` int(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` smallint(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`course_id`, `course_name`, `min_grade`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Bachelor of Science in Accountancy', NULL, '2019-03-05 06:33:02', '2019-03-05 06:33:02', 1),
(2, 'Bachelor of Science in Business Administration Major in Human Resource Development Management', NULL, '2019-03-05 06:34:12', '2019-03-05 06:34:12', 1),
(3, 'Bachelor of Science in Business Administration Major in Marketing Management', NULL, '2019-03-05 06:35:12', '2019-03-05 06:35:12', 1),
(4, 'Bachelor of Science in Computer Science', NULL, '2019-03-05 06:36:12', '2019-03-05 06:36:12', 1),
(5, 'Bachelor of Science in Information Technology', NULL, '2019-03-05 06:36:36', '2019-03-05 06:36:36', 1),
(6, 'Bachelor of Science in Entertainment and Multimedia Computing Specializing in Game Development', NULL, '2019-03-05 06:37:28', '2019-03-05 06:37:28', 1),
(7, 'Bachelor of Science in Entertainment and Multimedia Computing Specializing in Digital Animation Technology', NULL, '2019-03-05 06:37:58', '2019-03-05 06:37:58', 1),
(8, 'Bachelor of Arts in Communication', NULL, '2019-03-05 06:38:30', '2019-03-05 06:38:30', 1),
(9, 'Bachelor of Science in Psychology', NULL, '2019-03-05 06:38:49', '2019-03-05 06:38:49', 1),
(10, 'Bachelor of Elementary Education With specialization in Early Childhood Education', NULL, '2019-03-05 06:40:07', '2019-03-05 06:40:07', 1),
(11, 'Bachelor of Secondary Education Major in English', NULL, '2019-03-05 06:40:43', '2019-03-05 06:40:43', 1),
(12, 'Bachelor of Secondary Education Major in Mathematics', NULL, '2019-03-05 06:41:13', '2019-03-05 06:41:13', 1),
(13, 'Teacher Certificate Program (TCP)', NULL, '2019-03-05 06:42:05', '2019-03-05 06:42:05', 1),
(14, 'Bachelor of Science in Computer Engineering', NULL, '2019-03-05 06:42:28', '2019-03-05 06:42:28', 1),
(15, 'Bachelor of Science in Electronics Engineering', NULL, '2019-03-05 06:42:54', '2019-03-05 06:42:54', 1),
(16, 'Bachelor of Science in Electrical Engineering', NULL, '2019-03-05 06:43:12', '2019-03-05 06:43:12', 1),
(17, 'Bachelor of Science in Industrial Engineering', NULL, '2019-03-05 06:43:32', '2019-03-05 06:43:32', 1),
(18, 'Bachelor of Science in Mechanical Engineering', NULL, '2019-03-05 06:43:46', '2019-03-05 06:43:46', 1),
(19, 'Bachelor of Science in Nursing', NULL, '2019-03-05 06:44:06', '2019-03-05 06:44:06', 1),
(20, 'Bachelor of Science in Hospitality Management', NULL, '2019-03-05 06:44:37', '2019-03-05 06:44:37', 1),
(21, 'Bachelor of Science in Tourism Management', NULL, '2019-03-05 06:44:54', '2019-03-05 06:44:54', 1),
(22, 'Bachelor of Science in Entrepreneurship', NULL, '2019-03-05 06:45:29', '2019-03-05 06:45:29', 1),
(23, 'Bachelor of Science in Architecture', NULL, '2019-03-05 06:45:48', '2019-03-05 06:45:48', 1),
(24, 'Bachelor of Science in Criminology', NULL, '2019-03-05 06:46:07', '2019-03-05 06:46:07', 1),
(25, 'Bachelor of Science in Business Administration Major in Financial Management', NULL, '2019-03-05 06:46:55', '2019-03-05 06:46:55', 1);

-- --------------------------------------------------------

--
-- Table structure for table `exam`
--

CREATE TABLE `exam` (
  `exam_id` int(10) NOT NULL,
  `exam_name` varchar(500) NOT NULL,
  `exam_desc` text DEFAULT NULL,
  `status` smallint(5) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `feedback_id` int(10) NOT NULL,
  `reference_number` int(15) NOT NULL,
  `feedback` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `permit_key`
--

CREATE TABLE `permit_key` (
  `id` int(10) NOT NULL,
  `permit_key` varchar(10) NOT NULL,
  `reference_number` varchar(15) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permit_key`
--

INSERT INTO `permit_key` (`id`, `permit_key`, `reference_number`, `created_at`, `updated_at`) VALUES
(3, 'MaSQm', '201918575', '2019-03-09 13:38:57', '2019-03-09 13:38:57');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `question_id` int(10) NOT NULL,
  `question_desc` text NOT NULL,
  `question_choices` text NOT NULL,
  `question_answer` text NOT NULL,
  `subject_name` varchar(100) NOT NULL,
  `exam_id` int(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` smallint(2) NOT NULL,
  `courses` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`question_id`, `question_desc`, `question_choices`, `question_answer`, `subject_name`, `exam_id`, `created_at`, `updated_at`, `status`, `courses`) VALUES
(1, '[\"In Manila, It only _____ two days to get a phone installed. \",\" Read courtesy is often encouraged but ___ practiced. \",\"I\'m very happy _____ in India. I really miss being there.\",\"question 4She was working on her computer with her baby next to _____.\",\"_____ in trying to solve this problem. It\'s clearly unsolvable.\",\"question 6\",\"question 7\"]', '[[\"needs\",\"gets\",\"takes\",\"passes\"],[\"always\",\"mostly\",\"daily\",\"occasionally\"],[\"to live\",\"to have lived\",\"to has lived\",\"to lived\"],[\"herself\",\"her own\",\"hers\",\"her is\"],[\"There\'s no point\",\" It\'s no point\",\"There isn\'t point\",\"It\'s no need\"],[\"choices 1\",\"choices 2\",\"choices 3\",\"choices 4\"],[\"choices 1\",\"choices 2\",\"choices 3\",\"choices 4\"]]', '[\"A\",\"A\",\"D\",\"C\",\"A\",\"A\",\"A\"]', 'English', 0, '2019-03-06 10:54:45', '2019-03-15 15:13:26', 0, '[[\"1\",\"3\"],[\"3\",\"6\",\"8\",\"9\"],[\"1\"],[\"3\"],[\"10\"],[\"14\"],[\"11\",\"15\"]]'),
(2, '[\"What happens when two tectonic plates push against each other?\",\"What kinds of waves are used to make and receive cellphone calls?\",\"Which of these is the main way that ocean tides are created?\",\"What does a light-year measure?\",\"The loudness of a sound is determined by what property of a sound wave? \",\"Which of these elements is needed to make nuclear energy and nuclear weapons?\",\"Which of these terms is defined as the study of how the positions of stars and planets can influence human behavior?\",\"Which of these people developed the polio vaccine?\",\"A mole is a measurement that can be based on a common enumeration of:\",\"Light can be described as...\",\"Silicon is useful in microprocessors because it is a:\",\"Diamonds are:  \",\"What are the four basic forces  of nature? \",\"O+ blood can be transfused into people with which blood types?\",\"Which nutrient contains more calories per gram?\",\"Which of these is not one of the three main rock types?\",\"Which of these is NOT a system in the human body? \",\"Which solar system planets have rings?\",\"What percentage of animal species are invertebrates?\",\"Which of these is not a correct DNA base pairing?\",\"What comes next? Kingdom, Phylum, Class, _____.\",\"Which of these is NOT found in an animal cell?\",\"Which of these is NOT part of the digestive system?\",\"What is the name of the most recent supercontinent?\",\"How many atoms are in a mole?\",\"Which of these is another way to write Newton\'s second law of motion?\",\"Which of these is another way to write Newton\'s second law of motion?\",\"Which metal is used in the making of microchips?\",\"Who invented periodic table? \",\"Who is honored as Father of Modern Chemistry?\",\"Which gases cause acid rain?\",\"What is sodium chloride?\",\"What is the chemical symbol for oxygen?\",\"What are different masses of an atom in the same element called?\",\"On a molecular level, why is water such a good solvent?\",\"The atom is made of:\",\"Which of the following is not a good heat insulator?\",\"Light travels in a:\",\"The rate of change of position in a fixed direction.\",\"Which of the following are known as \\\"pisces\\\"?\",\"Bile is secreted by the\",\"A substance that absorbs moisture is known as:\",\"Chemical symbol AU stands for:\",\"What type of consumer only eats plants?\",\"Entomology is the study of:\",\"Paleontology is the study of:\",\"Botanical name of rice is:\",\"What type of clouds have a sheetlike appearance and are composed of ice crystals?\",\"What types of clouds tend to be in groups and have a light gray color?\",\"Which type of clouds cover the entire sky?\"]', '[[\"Earthquake\",\"Sandstorm\",\"Sinkhole\",\"Tsunami\"],[\"Radio waves\",\"Visible light waves\",\"Sound waves\",\"Gravity waves\"],[\"The rotation of the Earth on its axis\",\"The gravitational pull of the moon\",\"The gravitational pull of the sun\",\"The gravitational pull of the star\"],[\"Brightness\",\"Time\",\"Distance\",\"Weight\"],[\"Frequency\",\"Wave length\",\"Velocity or rate of change\",\"Amplitude or height\"],[\"Sodium chloride\",\"Uranium\",\"Nitrogen\",\"Carbon Dioxide\"],[\"Astrology\",\"Alchemy\",\"Astronomy\",\"Meteorology\"],[\"Marie Curie\",\"Isaac Newton\",\"Albert Einstein\",\"Jonas Salk \"],[\"Mass\",\"Atoms\",\"Protons\",\"Weight\"],[\"A stream of particles\",\"A wave\",\"A vibration in the space-time continuum\",\"Both particle and wavelike\"],[\"Superconductor\",\"Semiconductor\",\"Insulator\",\"Non-conductor\"],[\"A form of silicon created by extreme pressure.\",\"A form of lithium created by extreme heat.\",\"A form of carbon created by extreme pressures.\",\"A form of silicon created by fossilization.\"],[\"Gravitational, Electromagnetic, Kinematic, Strong\",\"Motion, Potential Energy, Enertia, Momentum\",\"Strong, Weak, Gravitational, Electromagnetic\",\"Earth, Air, Wind, Fire\"],[\"Any blood type\",\"Only other O+ blood types.\",\"Any blood type that is RH+.\",\"Any blood type that is RH-\"],[\"Fat\",\"Protein\",\"Alcohol\",\"Meat\"],[\"Sedimentary\",\"Metamorphic\",\"Igneous\",\"Basaltic\"],[\"The nervous system\",\"The endocrine system\",\"The cellular system\",\"The skeletal system\"],[\"Saturn and Jupiter\",\"Saturn, Jupiter, and Uranus\",\"Saturn, Jupiter Neptune, and Uranus\",\"Saturn, Uranus\"],[\"About 25%\",\"About 50%\",\"About 90%\",\"About 95%\"],[\"Adenine (A) and thymine (T)\",\"Adenine (A) and guanine (G)\",\"Cytosine (C) and guanine (G)\",\"Cytosine (C) and thymine (T)\"],[\"Order\",\"Genus\",\"Species\",\"Family\"],[\"Mitochondria\",\"Nucleus\",\"Chloroplasts\",\"Endoplasmic reticulum\"],[\"Duodenum\",\"Large Intestine\",\"Diaphragm\",\"Pericardium\"],[\"Rodinia\",\"Gondwana\",\"Pangea\",\"Pannotia\"],[\"6.022x10^21\",\"6.022x10^22\",\"6.022x10^23\",\"6.022x10^24\"],[\"F = ma\",\"F = m/a\",\"F = m + a\",\"F = m - a\"],[\"Reflection\",\"Refraction\",\"Diffraction\",\"Interaction\"],[\"Silicon\",\"Copper\",\"Brass\",\"Aluminum\"],[\"John Newlands\",\"Antoine Lavoisier\",\"John Dalton\",\"Dmitri Mendeleev \"],[\"Antoine Lavoisier\",\"Aristotle\",\"Theophrastus\",\"Antonie Philips\"],[\"Sulphur dioxide, Nitrogen oxides\",\"Freon\",\"Hydrogen sulfide\",\"Propane\"],[\"Sugar\",\"Table Salt\",\"Pepper\",\"Powder\"],[\"O \",\"On\",\"Ox\",\"N\"],[\"Isotopes\",\"Atomic mass\",\"Radioisotopes\",\"Electronegativity\"],[\"It is non-polar\",\"It is polar\",\"It contains hydrogen and hydrogen itself is a good solvent.\",\"It is wet\"],[\"Positrons, Neutrons and Electrons\",\"Neutrinos, Gamma Rays and Positrons\",\"Protons, Neutrons and Electrons\",\"Protons and Quarks\"],[\"Feathers\",\"Wool\",\"Iron\",\"Wood\"],[\"Circular Path\",\"Parabolic Path\",\"Straight Line\",\"Hyperbolic Path\"],[\"Acceleration\",\"Speed\",\"Momentum\",\"Velocity\"],[\"Reptiles\",\"Mammals\",\"Fish\",\"Birds\"],[\"Liver\",\"Lungs\",\"Gall Bladder\",\"Stomach\"],[\"Amorphous\",\"Hydrophilic\",\"Hygroscopic\",\"Hydrophobic\"],[\"Carbon\",\"Potassium\",\"Gold\",\"Phosphorus\"],[\"Carnivore\",\"Herbivore\",\"Omnivore\",\"Both Herbivore and Omnivore\"],[\"Insects\",\"Bones\",\"Fishes\",\"Birds\"],[\"Viruses\",\"Poisons\",\"Fossils\",\"Medicine\"],[\"Oryza sativa\",\"Zea mays\",\"Glycine max\",\"Poaceae\"],[\"Stratocumulus Clouds\",\"Cirrostratus Clouds\",\"Nimbostratus Clouds\",\"Altostratus Clouds \"],[\"High level clouds\",\"Middle level clouds\",\"Low level clouds\",\"All of the above\"],[\"Cirrus Clouds\",\"Altocumulus Clouds\",\"Nibostratus Clouds\",\"Altostratus Clouds\"]]', '[\"A\",\"A\",\"B\",\"C\",\"D\",\"B\",\"A\",\"D\",\"B\",\"D\",\"B\",\"C\",\"C\",\"C\",\"A\",\"D\",\"C\",\"C\",\"D\",\"B\",\"A\",\"C\",\"D\",\"C\",\"C\",\"A\",\"B\",\"A\",\"D\",\"D\",\"A\",\"B\",\"A\",\"A\",\"B\",\"C\",\"C\",\"C\",\"B\",\"C\",\"A\",\"C\",\"C\",\"B\",\"A\",\"C\",\"A\",\"B\",\"B\",\"D\"]', 'Science', 0, '2019-03-06 10:55:53', '2019-03-10 05:38:49', 0, '[[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"15\",\"16\",\"18\",\"19\"],[\"15\",\"16\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"15\",\"16\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"15\",\"16\",\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"],[\"19\"]]'),
(3, '[\"-2x (x + 3) - (x + 1) (x - 2) =\",\"x / (x + 2) – 7 / (x - 2) =\",\"?(x²) =\",\"(5?²x?¹) / (x?y²) =\",\"(64)^(-1/2)=\",\"If ?(x + a = b), then x =\",\"|2x + 7| ? 1 is equivalent to which of the following?\",\"a^(2/3)  ? a^(1/4) =\",\"If f(x) = 4x - 2, then f(x - 1) =\",\"The graph of which of the following equations is a line parallel to the graph of x - 5y = 8?\",\"If ?(x + b) = a, then x =\",\"A parent rewards a child with 50 cents for each correctly solved mathematics problem and fines the child 30 cents for each incorrectly solved problem. If the child nets $22.00 after 100 problems. How many problems were solved correctly?\",\"If 3^(x+5) = 1/27, then x =\",\"If x ? 0, then ?(x? - 6x² + 9) =\",\"If log? 81 = x, then x =\",\"If log5 = 0.6990 and log3  = 0.4771, then log45 =\",\"If logx = 3loga + logb, then x =\",\"If 2^1.5894 = 3, then 2^4.5894 =\",\"Given that a circle has 2? radians, how many degrees are equal to ?/12 radians?\",\"If cos60? = 1/2, then cos300? =\",\"If tan? = 5/12, 0? ? ? ? 90?, then cos? =\",\"Given 0? ? x ? 360? and 4sinx - 1 = -5, then x =\",\"2sinxcosx =\",\"sin?(csc? - sin?) =\",\"Which expresses y as a function of x?\",\"Let f(x) = x² - 3/x, g(x) = ?(x + 7). Find f(g(2)).\",\"Solve tan²x = 1, where 0 ? x ? ?.\",\"Solve: 49x? - 25x² = 0\",\"What is the remainder when x³ + 19x² + 114x + 218 is divided by (x + 4)?\",\"The sum of a number and 6 is 8 more than twice the number. Find the equation that could be used to find this number x.\",\"Given that f(x) is one-to-one, find the inverse of the function f(x) = (x - 6)³ + 6.\",\"Write an equation of the line passing through the point (6, -10) with an undefined slope.\",\"Perform the indicated operation and write the result in standard form: (-3 + 2i)(-3 - 7i).\",\"If Log? (1 / 8) = - 3 / 2, then x is equal to\",\"20% of 2 is equal to\",\"If Log? (x) = 12, then log? (x / 4) is equal to\",\"The population of a country increased by an average of 2% per year from 2000 to 2003. If the population of this country was 2,000,000 on December 31, 2003, then the population of this country on January 1, 2000, to the nearest thousand would have been\",\"f is a quadratic function whose graph is a parabola opening upward and has a vertex on the x-axis. The graph of the new function g defined by g(x) = 2 - f(x - 5) has a range defined by the interval\",\"f is a function such that f(x) < 0. The graph of the new function g defined by g(x) = | f(x) | is a reflection of the graph of f\",\"If the graph of y = f(x) is transformed into the graph of 2y - 6 = - 4 f(x - 3), point (a , b) on the graph of y = f(x) becomes point (A , B) on the graph of 2y - 6 = - 4 f(x - 3) where A and B are given by\",\"When a parabola represented by the equation y - 2x² = 8 x + 5 is translated 3 units to the left and 2 units up, the new parabola has its vertex at\",\"The graphs of the two linear equations ax + by = c and bx - ay = c, where a, b and c are all not equal to zero,\",\"The graphs of the two equations y = ax² + bx + c and y = Ax² + Bx + C, such that a and A have different signs and that the quantities b² - 4ac and B² - 4AC are both negative,\",\"For x greater than or equal to zero and less than or equal to 2 ?, sin x and cos x are both decreasing on the intervals\",\"The three solutions of the equation f(x) = 0 are -2, 0, and 3. Therefore, the three solutions of the equation f(x - 2) = 0 are\",\"A school committee consists of 2 teachers and 4 students. The number of different committees that can be formed from 5 teachers and 10 students is\",\"If f(x) is an odd function, then | f(x) | is\",\"The period of | sin (3x) | is \",\"When a metallic ball bearing is placed inside a cylindrical container, of radius 2 cm, the height of the water, inside the container, increases by 0.6 cm. The radius, to the nearest tenth of a centimeter, of the ball bearing is\",\"The period of 2 sin x cos x is\"]', '[[\"-x² - 7x -2\",\"-x² + 5x + 2\",\"-3x² - 7x + 2\",\"-3x² - 5x + 2\"],[\"(x - 7) / (x + 2)\",\"(x + 7) / (x + 2)\",\"(x² - 9x - 14) / (x² - 4)\",\"(x² - 9x + 14) / (x² - 4)\"],[\"x^(2/3)\",\"x?³\",\"x^(3/2)\",\"x??\"],[\"1 / (25x³y²)\",\"1 / (10x?y²)\",\"(25x?) / y²\",\"1 / (25x?y²)\"],[\"8\",\"- 4\",\"-1/2\",\"1/8\"],[\"(b – a)³\",\"(a – b)³\",\"b³ – a³\",\"a³ – b³\"],[\"X ? –3\",\"X ? 3\",\"3 ? x ? 4\",\"–4 ? x ? –3\"],[\"a^(1/6)\",\"a^(2/7)\",\"a^(11/12)\",\"a^(1/4)\"],[\"4x² - 6x + 2\",\"4x² + 2x + 2\",\"4x + 2\",\"4x - 6 \"],[\"x + 5y = 8\",\"5x - y = 8\",\"2x + 10y = 8\",\"2x - 10y = 8\"],[\"a - b²\",\"a - ?b\",\"a² - b\",\"a² + b\"],[\"30\",\"65\",\"45\",\"53\"],[\"-10\",\"- 8\",\"- 6\",\"-2\"],[\"x² - ?6x + 3\",\"x² + ?6x + 3\",\"|x² - 3|\",\"x² ?(6x + 9)\"],[\"2\",\"4\",\"6\",\"12\"],[\"0.93755\",\"0.58881\",\"2.3524\",\"1.6532\"],[\"(ab)³\",\"3a+b\",\"3ab\",\"a³b\"],[\"18\",\"24\",\"28\",\"30\"],[\"6?\",\"12?\",\"15?\",\"20?\"],[\"1/2\",\"- 1/2\",\"?3/2\",\"?2/?2\"],[\"5/13\",\"12/13\",\"13/5\",\"13/12\"],[\"0?\",\"90?\",\"180?\",\"270?\"],[\"sin2x\",\"cos2x\",\"?4sinxcosx\",\"-2cos²x\"],[\"cos²?\",\"tan? - sin²?\",\"sin²?\",\"sec? - sin²?\"],[\"x² + y² = 4\",\"x = y² - 4\",\"x = 4\",\"x² + 4y = 4\"],[\"6\",\"12\",\"8\",\"0\"],[\"?/2\",\"?/4, 3?/4\",\"1, -1\",\"0\"],[\"x = ±25/49\",\"x = 0, ±5/7\",\"x = ±49/25\",\"x = ±5/7\"],[\"4\",\"9\",\"6\",\"2\"],[\"x + 6 = 2x + 8\",\"x + 6 = x² + 8\",\"x + 6 = 2(x + 8)\",\"x + 14 = 2x\"],[\"f?¹(x) = ?(x - 6) + 6\",\"f?¹(x) = ?x + 6\",\"f?¹(x) = ?(x + 6) - 6\",\"None of these\"],[\"x = -10\",\"x = 6\",\"y = 6\",\"y = -10\"],[\"-5 + 27i\",\"23 + 15i\",\"-5 + 15i\",\"23 - 15i\"],[\"- 4\",\"4\",\"1/4\",\"10\"],[\"20\",\"4\",\"0.4\",\"0.04\"],[\"11\",\"48\",\"-12\",\"22\"],[\"1,846,000\",\"1,852,000\",\"1,000,000\",\"1,500,000\"],[\"[ -5 , + infinity)\",\"[ 2 , + infinity)\",\"( - infinity , 2] \",\"( - infinity , 0]\"],[\"on the y axis \",\"on the x axis\",\"on the line y = x\",\"on the line y = - x\"],[\"A = a - 3, B = b\",\"A = a + 3, B = b \",\"A = a + 3, B = -2 b\",\"A = a + 3, B = -2 b +3\"],[\"(-5 , -1)\",\"(-5 , -5) \",\"(-1 , -3) \",\"(-2 , -3)\"],[\"are parallel\",\"intersect at one point\",\"intersect at two points\",\"perpendicular\"],[\"intersect at two points \",\"intersect at one point \",\"do not intersect \",\"none of the above\"],[\"(0, ?/2)\",\"(?/2, ?) \",\"(?, 3 ? / 2) \",\"(3 ? / 2, 2 ?)\"],[\"- 4, -2, and 1\",\"-2, 0 and 3 \",\"4, 2, and 5\",\"0, 2 and 5\"],[\"10\",\"15\",\"2100\",\"8\"],[\"an odd function \",\"an even function \",\"neither odd nor even \",\"even and odd\"],[\"2 ? \",\"2 ? / 3 \",\"? / 3 \",\"3 ?\"],[\"1 cm\",\"1.2 cm \",\"2 cm \",\"0.6 cm\"],[\"4 ? 2\",\"2 ? \",\"4 ? \",\"?\"]]', '[\"D\",\"C\",\"A\",\"D\",\"D\",\"A\",\"D\",\"C\",\"D\",\"D\",\"C\",\"B\",\"B\",\"C\",\"B\",\"D\",\"D\",\"B\",\"C\",\"A\",\"B\",\"D\",\"A\",\"A\",\"D\",\"C\",\"B\",\"B\",\"D\",\"A\",\"A\",\"B\",\"B\",\"B\",\"C\",\"D\",\"A\",\"C\",\"B\",\"D\",\"A\",\"D\",\"C\",\"B\",\"D\",\"C\",\"B\",\"C\",\"B\",\"D\"]', 'Math', 0, '2019-03-06 10:59:00', '2019-03-15 02:43:02', 0, '[[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"],[\"4\",\"5\",\"12\",\"14\",\"15\",\"16\",\"17\",\"18\",\"23\"]]');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `subject_id` int(11) NOT NULL,
  `subject_name` varchar(100) NOT NULL,
  `status` int(2) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`subject_id`, `subject_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'English', 1, '2019-02-24 11:51:42', '2019-02-26 23:44:19'),
(2, 'Science', 1, '2019-02-24 12:11:51', '2019-02-26 23:44:25'),
(3, 'Math', 1, '2019-02-26 23:47:14', '2019-02-26 23:47:14');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(255) NOT NULL,
  `reference_number` varchar(20) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) NOT NULL,
  `contact_number` varchar(12) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `campus` varchar(500) DEFAULT NULL,
  `suffix` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `reference_number`, `first_name`, `middle_name`, `last_name`, `contact_number`, `email`, `password`, `role`, `status`, `created_at`, `updated_at`, `campus`, `suffix`) VALUES
(1, 'karlfule', 'Karl', 'test', 'Fule', '12312312', 'karlfule@gmail.com', 'pass1234', 'Guidance Personnel', 1, '2019-02-23 06:58:18', '2019-03-06 10:47:09', NULL, 't'),
(10, 'mis', 'Karl', 'test', 'Fule', '12312312', 'karlfule_mis@gmail.com', 'pass1234', 'MIS', 1, '2019-02-23 06:58:18', '2019-03-06 10:47:09', NULL, 't');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `application`
--
ALTER TABLE `application`
  ADD PRIMARY KEY (`application_id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`course_id`);

--
-- Indexes for table `exam`
--
ALTER TABLE `exam`
  ADD PRIMARY KEY (`exam_id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`feedback_id`);

--
-- Indexes for table `permit_key`
--
ALTER TABLE `permit_key`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permit_key` (`permit_key`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`question_id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`subject_id`),
  ADD UNIQUE KEY `subject_name` (`subject_name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reference_number` (`reference_number`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `application`
--
ALTER TABLE `application`
  MODIFY `application_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `course_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `exam`
--
ALTER TABLE `exam`
  MODIFY `exam_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `feedback_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permit_key`
--
ALTER TABLE `permit_key`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `question_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `subject_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
