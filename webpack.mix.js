let mix = require('laravel-mix');
mix.setPublicPath('dist');
mix.webpackConfig({
    resolve: {
        alias: { 'vuejs-datatable': 'vuejs-datatable/dist/vuejs-datatable.esm.js' }
    }
});
//mix.js('./assets/src/js/app.js', './assets/src/js/dist/app.js');
mix.js('./assets/src/js/admin.js', './assets/src/js/admin.js');