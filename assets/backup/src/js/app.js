
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
//import App from './App.vue'
import AdminFooter from './components/AdminFooter.vue'
import AdminSidebar from './components/AdminSidebar.vue'
import AdminHeader from './components/AdminHeader.vue'
import AdminContent from './components/AdminContent.vue'
// import 'font-awesome/less/font-awesome.less';


/* eslint-disable no-new */
/*new Vue({
  el: '#app',
  template: '<App/>',
  components: { App }
})*/

new Vue({
  el: '#footer',
  template: '<AdminFooter/>',
  components: { AdminFooter }
})

new Vue({
  el: '#sidebar',
  template: '<AdminSidebar/>',
  components: { AdminSidebar }
})

new Vue({
  el: '#header',
  template: '<AdminHeader/>',
  components: { AdminHeader }
})


new Vue({
  el: '#content',
  template: '<AdminContent/>',
  components: { AdminContent },
  data:{
  	message: 'jonille ulul'
  }
})
