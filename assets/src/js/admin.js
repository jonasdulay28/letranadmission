//Vue
import Vue from 'vue'
//router
import VueRouter from 'vue-router'
//components
import Admin_Dashboard from './Admin_Dashboard.vue'
import Suggestions from 'v-suggestions'
//import 'v-suggestions/dist/v-suggestions.css' // you can import the stylesheets also (optional)

window.Vue = Vue;
//axios
import VueAxios from 'vue-axios'
import axios from 'axios'
import Dashboard from './admin/Dashboard.vue'
import Add_Work_History from './admin/Work_history/Add.vue'
import Edit_Work_History from './admin/Work_history/Edit.vue'
import View_Work_History from './admin/Work_history/View.vue'

import View_Users from './admin/Users/View.vue'
import Add_Users from './admin/Users/Add.vue'
import Edit_Users from './admin/Users/Edit.vue'

import View_Courses from './admin/Courses/View.vue'
import Add_Course from './admin/Courses/Add.vue'
import Edit_Course from './admin/Courses/Edit.vue'

import View_Applications from './admin/Applications/View.vue'
import Edit_Applications from './admin/Applications/Edit.vue'

import View_Feedbacks from './admin/Feedbacks/View.vue'
import Edit_Feedbacks from './admin/Feedbacks/Edit.vue'


import Manage_Subjects from './admin/Subjects/Manage.vue'

import Manage_Exams from './admin/Exams/Manage.vue'
import Edit_Exams from './admin/Exams/Edit.vue'
import Submitted_Exams from './admin/Exams/Submitted_Exams.vue'
import View_Exam from './admin/Exams/View.vue'
import Manage_Settings from './admin/Exams/Settings.vue'

import View_Reports from './admin/Reports/View_Reports.vue'

import Add_Prof_Summary from './admin/Prof_Summary/Add.vue'
import Edit_Prof_Summary from './admin/Prof_Summary/Edit.vue'
import View_Prof_Summary from './admin/Prof_Summary/View.vue'
import Navbar from './admin/Header.vue'
import Footer from './admin/Footer.vue'
import Sidebar from './admin/Sidebar.vue'
import VueCkeditor from 'vue-ckeditor2';
import DatatableFactory from 'vuejs-datatable';


Vue.use(VueCkeditor);

window.axios = axios
Vue.use(VueAxios, axios)
Vue.use(VueRouter);
Vue.use(Suggestions);
Vue.use(DatatableFactory);
Vue.component('vue-ckeditor',VueCkeditor);
Vue.component('admin_header', Navbar);
Vue.component('admin_footer', Footer);
Vue.component('admin_sidebar', Sidebar);
/*export const resumeBus = new Vue();*/

const routes = [
	{
		name: '/',
		path: '/',
		component: Dashboard
	},
	{
		name: '/view_reports',
		path: '/view_reports',
		component: View_Reports
	},
	{
		name: '/view_users',
		path: '/view_users',
		component: View_Users
	},
	{
		name: '/add_users',
		path: '/add_users',
		component: Add_Users
	},
	{
		name: '/edit_user',
		path: '/edit_user/:id',
		component: Edit_Users
	},
	{
		name: '/view_applications',
		path: '/view_applications',
		component: View_Applications
	},
	{
		name: '/edit_application',
		path: '/edit_application/:id',
		component: Edit_Applications
	},
	{
		name: '/manage_subjects',
		path: '/manage_subjects',
		component: Manage_Subjects
	},
	{
		name: '/manage_exams',
		path: '/manage_exams',
		component: Manage_Exams
	},
	{
		name: '/manage_settings',
		path: '/manage_settings',
		component: Manage_Settings
	},
	{
		name: '/submitted_exams',
		path: '/submitted_exams',
		component: Submitted_Exams
	},
	{
		name: '/view_exam',
		path: '/view_exam/:id',
		component: View_Exam
	},
	{
		name: '/edit_exam',
		path: '/edit_exam/:id',
		component: Edit_Exams
	},
	{
		name: '/view_courses',
		path: '/view_courses',
		component: View_Courses
	},
	{
		name: '/add_course',
		path: '/add_course',
		component: Add_Course
	},
	{
		name: '/edit_course',
		path: '/edit_course/:id',
		component: Edit_Course
	},
	{
		name: '/view_feedbacks',
		path: '/view_feedbacks',
		component: View_Feedbacks
	}
	/*,
	{
		name: '/add_work_history',
		path: '/add_work_history',
		component: Add_Work_History
	},
	{
		name: '/view_work_history',
		path: '/view_work_history',
		component: View_Work_History
	},
	{
		name: '/edit_work_history',
		path: '/edit_work_history/:id',
		component: Edit_Work_History,
	},
	{
		name: '/add_prof_summary',
		path: '/add_prof_summary',
		component: Add_Prof_Summary
	},
	{
		name: '/view_prof_summary',
		path: '/view_prof_summary',
		component: View_Prof_Summary
	},
	{
		name: '/edit_prof_summary',
		path: '/edit_prof_summary/:id',
		component: Edit_Prof_Summary,
	}*/
];
const router = new VueRouter({
	routes,
	linkExactActiveClass: "active" // active class for *exact* links.,
	//,mode: 'history'
});


new Vue({
    el: '#app',
    router,
    render: app => app(Admin_Dashboard)
});