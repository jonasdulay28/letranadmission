CI VUE Tutorial
Requirements
NodeJS im using v9.5
NPM  v5.6

1. Create a package.json, run npm init
2. run npm install laravel-mix cross-env node-sass --save-dev
3. add this to the package.json
"scripts": {
  "dev": "node node_modules/cross-env/dist/bin/cross-env.js NODE_ENV=development node_modules/webpack/bin/webpack.js --progress --hide-modules --config=node_modules/laravel-mix/setup/webpack.config.js",
  "watch": "node node_modules/cross-env/dist/bin/cross-env.js NODE_ENV=development node_modules/webpack/bin/webpack.js --watch --progress --hide-modules --config=node_modules/laravel-mix/setup/webpack.config.js",
  "hot": "node node_modules/cross-env/dist/bin/cross-env.js NODE_ENV=development node_modules/webpack-dev-server/bin/webpack-dev-server.js --inline --hot --config=node_modules/laravel-mix/setup/webpack.config.js",
  "production": "node node_modules/cross-env/dist/bin/cross-env.js NODE_ENV=production node_modules/webpack/bin/webpack.js --progress --hide-modules --config=node_modules/laravel-mix/setup/webpack.config.js"
}

//Install Vue Dependencies
4. 
- npm i vue
- npm i axios
- npm i vue-axios
- npm i vue-router
- npm install cross-env node-sass --save-dev

5 Create a file webpack.mix.js
let mix = require('laravel-mix');
mix.setPublicPath('dist');
mix.js('./assets/src/js/app.js', './assets/src/js/app.js');
6. Create a dist folder in the root dir
7. Create a dir assets/src/js/app.js
8. In the app.js 
//Vue
import Vue from 'vue'
//router
import VueRouter from 'vue-router'
//components
import App from './App.vue'
//axios
import VueAxios from 'vue-axios'
import axios from 'axios'

Vue.use(VueAxios, axios)
Vue.use(VueRouter);

new Vue({
    el: '#app',
    render: app => app(App)
});

9. In the controller then to the view create div#app
then import the compiled file.



